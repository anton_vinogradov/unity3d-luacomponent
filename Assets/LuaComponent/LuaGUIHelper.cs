﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using LuaInterface;
using System.Collections;
using System.Collections.Generic;

public class LuaGUIHelper : MonoBehaviour {
	public static void Label(string aText) {
		GUILayout.Label(aText);
	}
	public static void LabelH(string aText, float aHeight) {
		GUILayout.Label(aText, GUILayout.Height(aHeight));
	}
	public static void LabelW(string aText, float aWidth) {
		GUILayout.Label(aText, GUILayout.Width(aWidth));
	}
	public static void LabelWH(string aText, float aWidth, float aHeight) {
		GUILayout.Label(aText, GUILayout.Width(aWidth), GUILayout.Height(aHeight));
	}
	public static void LabelS(string aText, GUIStyle aStyle) {
		GUILayout.Label(aText, aStyle);
	}
//	public static void LabelW(string aText, GUIStyle aStyle)
//	{
//		UnityEngine.GUILayout.Label(aText, aStyle);
//	}
//	public static void LabelH(string aText, GUIStyle aStyle)
//	{
//		UnityEngine.GUILayout.Label(aText, aStyle);
//	}
//	public static void LabelWH(string aText, GUIStyle aStyle)
//	{
//		UnityEngine.GUILayout.Label(aText, aStyle);
//	}
	public static void BeginArea(float x, float y, float x2, float y2) {
		var rect = new Rect(x, y, x2, y2);
		GUILayout.BeginArea(rect);
	}
	public static void EndArea() {
		GUILayout.EndArea();
	}


	public static void BeginHorizontal() {
		GUILayout.BeginHorizontal();
	}
	public static void EndHorizontal() {
		GUILayout.EndHorizontal();
	}

	public static void BeginVertical() {
		GUILayout.BeginVertical();
	}
	public static void BeginVerticalW(float aWidth) {
		GUILayout.BeginVertical(GUILayout.Width(aWidth));
	}
	public static void EndVertical() {
		GUILayout.EndVertical();
	}

	public static void BeginVerticalEHF() {
		GUILayout.BeginVertical(GUILayout.ExpandHeight(false));
	}

	public static bool Button(string aText) {
		return GUILayout.Button(aText);
	}
	public static bool ButtonH(string aText, float aHeight) {
		return GUILayout.Button(aText, GUILayout.Height(aHeight));
	}
	public static bool ButtonWH(string aText, float aWidth, float aHeight) {
		return GUILayout.Button(aText, GUILayout.Width(aWidth), GUILayout.Height(aHeight));
	}

	public static Vector2 BeginScrollView(Vector2 aPos) {
		return GUILayout.BeginScrollView(aPos);
	}
	public static Vector2 BeginScrollViewHMH(Vector2 aPos, float aHeight, float aMaxHeight) {
		return GUILayout.BeginScrollView(aPos, GUILayout.Height(aHeight), GUILayout.MaxHeight(aMaxHeight));
	}
	public static void EndScrollView() {
		GUILayout.EndScrollView();
	}

	public static string TextArea(string aText) {
		return GUILayout.TextArea(aText);
	}
	public static string TextAreaEHT(string aText) {
		return GUILayout.TextArea(aText, GUILayout.ExpandHeight(true));
	}

	public static string TextField(string aText) {
		return GUILayout.TextField(aText);
	}
	public static string TextFieldH(string aText, float aHeight) {
		return GUILayout.TextField(aText, GUILayout.Height(aHeight));
	}
	public static string TextFieldW(string aText, float aWidth) {
		return GUILayout.TextField(aText, GUILayout.Width(aWidth));
	}
	public static string TextFieldWH(string aText, float aWidth, float aHeight) {
		return GUILayout.TextField(aText, GUILayout.Width(aWidth), GUILayout.Height(aHeight));
	}

	public static void BoxS(string aText, GUIStyle aStyle) {
		GUILayout.Box(aText, aStyle);
	}
	public static void BoxH(string aText, float aHeight) {
		GUILayout.Box(aText, GUILayout.Height(aHeight));
	}

	public static bool ToggleWH(bool aValue, string aText, float aWidth, float aHeight) {
		return GUILayout.Toggle(aValue, aText, GUILayout.Width(aWidth), GUILayout.Height(aHeight));
	}

	public static GUILayoutOption ExpandWidth(bool aValue) {
		return GUILayout.ExpandWidth(aValue);
	}

	public class AtlasTexture {
		public Texture2D texture ;
		public Rect rect ;

		public AtlasTexture(Texture2D texture, Rect rect) {
			this.texture = texture;
			this.rect    = rect;
		}
	};

//	static public AtlasTexture LoadAtlasTexture(string aPsdPath, string aSpriteName) {
//		kt.Psd psd = kt.Psd.create(aPsdPath + ".psd");
//		if (psd == null) {
//			return null;
//		}
//
//		kt.PsdResource.Layer info = psd.getLayer(aSpriteName);
//		if (info == null) {
//			return null;
//		}
//
//		UIAtlas atlas = kt.FileSystem.load<GameObject>(info.atlasName).GetComponent<UIAtlas>();
//
//		UISpriteData sprite = atlas.GetSprite(aSpriteName);
//		Texture2D texture = atlas.spriteMaterial.mainTexture as Texture2D;
//		Rect rect = new Rect(sprite.x, texture.height - sprite.y - sprite.height, sprite.width, sprite.height);
//
//		return new AtlasTexture(texture, rect);
//	}

	static public void DrawTexture(Rect aPosition, Texture aTexture, Rect aTextureRect) {
		Rect pos = aPosition;
		Rect texRect = aTextureRect;

		float coeff = 1.0f;

		if (texRect.width >= texRect.height) {
			coeff = pos.width / texRect.width;
		}
		else {
			coeff = pos.height / texRect.height;
		}

		pos.width = texRect.width * coeff;
		pos.height = texRect.height * coeff;

		texRect.x /= aTexture.width;
		texRect.y /= aTexture.height;
		texRect.width /= aTexture.width;
		texRect.height /= aTexture.height;

		UnityEngine.GUI.DrawTextureWithTexCoords(pos, aTexture, texRect);
	}

//	static public void DrawSpritePsd(Rect aPosition, string aPsdPath, string aSpriteName) {
//		AtlasTexture at = LoadAtlasTexture(aPsdPath, aSpriteName);
//		if (at != null) {
//			DrawTexture(aPosition, at.texture, at.rect);
//		}
//	}

	static public void DrawAtlasTexture(Rect aPosition, AtlasTexture aAtlasTex) {
		DrawTexture(aPosition, aAtlasTex.texture, aAtlasTex.rect);
	}


	#if UNITY_EDITOR
	public static float FloatField(string aLabel, float aValue) {
		return EditorGUILayout.FloatField(aLabel, aValue);
	}

	public static int IntField(string aLabel, int aValue) {
		return EditorGUILayout.IntField(aLabel, aValue);
	}
	public static Vector2 Vector2Field(string aLabel, Vector2 aValue) {
		if (aValue == null ) {
			aValue = Vector2.zero;
		}
		return EditorGUILayout.Vector2Field(aLabel, aValue);
	}

	public static Vector2 Vector2Field(string aLabel, object aValue) {
		if (aValue == null ) {
			aValue = new  Vector2();
		}
		return EditorGUILayout.Vector2Field(aLabel, (Vector2)aValue);
	}

	public static Color ColorField(string aLabel, Color aValue) {
		if (aValue == null ) {
			aValue = Color.white;
		}
		return EditorGUILayout.ColorField(aLabel, aValue);
	}

	public static string StringPopup(string aLabel, string aValue, LuaTable aOptions)
	{
		int curr = -1;
		var blocks = new List<string>();

		for (int i = 1; i <= aOptions.Keys.Count; i++) {
			var option = aOptions[i] as string;
			blocks.Add (option);
			if (option == aValue)
				curr = i - 1;
		}
		blocks.Add("none");

		curr = EditorGUILayout.Popup(aLabel, curr, blocks.ToArray());

		if (curr >= 0) {
			return blocks[curr];
		}

		return "none";
	}

	public static string StringPopupTree(string aLabel, string aValue, LuaTable aOptions, LuaTable aOptionsShow)
	{
		int curr = -1;
		var blocks = new List<string>();
		var showblocks = new List<string>();

		for (int i = 1; i <= aOptions.Keys.Count; i++) {
			var option = aOptions[i] as string;
			var optionShow = aOptionsShow[i] as string;
			blocks.Add (option);
			showblocks.Add (optionShow);
			if (option == aValue)
				curr = i - 1;
		}
		blocks.Add("none");
		showblocks.Add("none");
		curr = EditorGUILayout.Popup(aLabel, curr, showblocks.ToArray());

		if (curr >= 0) {
			return blocks[curr];
		}

		return "none";
	}

	public static string[] PrepareArrayPopup(LuaTable aOptions)
	{
		int count = aOptions.Keys.Count;
		string[] blocks = new string[count + 1];
		for (int i = 0; i < count; i++) {
			blocks[i] = aOptions[i + 1] as string;
		}
		blocks[count] = "none";
		return blocks;
	}


	#endif
}

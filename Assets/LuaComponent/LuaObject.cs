﻿using UnityEngine;
using System.Collections;
using LuaInterface;

public class LuaObject : MonoBehaviour 
{
	public LuaTable luaObject = null;



	void OnDestroy()
	{

		if (luaObject != null)
		{
			LuaFunction gc = (LuaFunction)luaObject["__gc"];
			gc.Call(luaObject);
		}
	}
}

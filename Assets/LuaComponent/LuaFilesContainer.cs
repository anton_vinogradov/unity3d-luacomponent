﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class LuaFilesContainer :ScriptableObject {
	[SerializeField]
	public List<string> pathname;
	[SerializeField]
	public List<string> script;
}

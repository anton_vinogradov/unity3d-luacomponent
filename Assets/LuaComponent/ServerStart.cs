﻿using UnityEngine;

public class ServerStart: MonoBehaviour{

	public string gameTypeName = "gtName";
	public string gameName = "gameName";
	public int playersMax = 100;
	public int port = 25000;
	void Awake () {

		bool useNat = !Network.HavePublicAddress();
		Network.InitializeServer(playersMax, port, useNat);
	}

	void Update () {

	}

	void OnMasterServerEvent(MasterServerEvent msEvent) {
		if (msEvent == MasterServerEvent.RegistrationSucceeded)
			Debug.Log(msEvent);

	}

	void OnServerInitialized() {
		MasterServer.RegisterHost( gameTypeName, gameName); 
		Debug.Log("Server initialized and ready");
	}

	void OnApplicationQuit() {
		Debug.Log("Network.Disconnect");
		Network.Disconnect();
		MasterServer.UnregisterHost();
	}

	void OnFailedToConnectToMasterServer(NetworkConnectionError info) {
		Debug.Log("Could not connect to master server: " + info);
	}

}

﻿using UnityEngine;
using System.Collections;

public class ClientStart : MonoBehaviour {

	NetworkView networkView;
	HostData connection;
	string debugstr  = "";
	float reconnecttimer = 1f;
	void Start () {
		MasterServer.ClearHostList();
		MasterServer.RequestHostList("RingUniverse");

//		networkView.RPC("ResultCallback", player, callbackid, answer);
	}
	
	void Update () {
		if (MasterServer.PollHostList().Length != 0) {
			HostData[] hostData = MasterServer.PollHostList();
			int i = 0;
			while (i < hostData.Length) {

				if (hostData[i].gameName== "MainRingUniverseServer" ){
					connection = hostData[i];
					debugstr += hostData[i].gameName;
				}
				i++;
			}
			MasterServer.ClearHostList();
		}

		if (!Network.isClient && Network.peerType != NetworkPeerType.Connecting){

			//Network.Connect("185.23.16.21", 25001);
			reconnecttimer -= Time.deltaTime;
			if (reconnecttimer <= 0 && connection != null){
		
				debugstr += Network.Connect(connection).ToString();
				reconnecttimer = 5;
			}
		} 

	}

	void OnApplicationQuit() {
		Network.Disconnect ();
	}
	void OnFailedToConnect(NetworkConnectionError error) {
		Debug.Log("Could not connect to server: " + error);
	}
	void OnConnectedToServer() {

		Debug.Log ("OnConnectedToServer : " +Network.player.guid);
	}
	void OnGUI () {
		GUILayout.Label (debugstr);
	}
}

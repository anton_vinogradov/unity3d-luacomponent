﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LuaInterface;
using System.IO;
using System.Text;
//using Vexe.Runtime.Types;
using Vexe.Runtime.Serialization;

public class LuaController : MonoBehaviour , ISerializationCallbackReceiver  {

	static BetterSerializer bs = new BetterSerializer ();
	public static string Serialize(object value){
		return bs.Serialize (value);
	}
	public static object Deserialize(string data, string type){
		return bs.Deserialize (System.Type.GetType (type), data, null);

	}

	public static LuaController instance ;

	public static LuaState lua ;

	public static LuaFunction registerClass;
	public static LuaFunction addModule;
	public static LuaFunction toLuaObject;
	public static LuaFunction inspectedDefGet;

	public static Dictionary<string, string> luaScripts= new Dictionary<string, string>();

	public static string engineLoad;
	public static string requiredModules;
	public static string starter;
	public static string dbfile;

	[SerializeField]
	public string s_engineLoad;
	[SerializeField]
	public string s_requiredModules;
	[SerializeField]
	public string s_starter;
	[SerializeField]
	public string s_dbfile;

	LuaFunction luaStart ;
	LuaFunction luaStop;
	LuaFunction luaUpdate ;
	LuaFunction luaOnGui;

	LuaFunction luaMouseDown ;
	LuaFunction luaMouseUp ;
	LuaFunction luaMouseMove ;

	public void OnBeforeSerialize(){
		s_engineLoad = engineLoad;
		s_requiredModules = requiredModules;
		s_starter = starter;
		s_dbfile = dbfile;
	}

	public void OnAfterDeserialize(){
		engineLoad = s_engineLoad;
		requiredModules = s_requiredModules;
		starter = s_starter;
		dbfile = s_dbfile;
	}


	void Awake () {
		LuaController.instance = this;
		Init ();
		if (!string.IsNullOrEmpty( starter)){
			LuaController.RequireLua (starter );
		}

		GetFunctions ();

		if (luaStart != null){
			luaStart.Call();
		}
	}

	void Start () {


	}

	public static object InspectedInit (string type) {
		return inspectedDefGet.Call(type)[0];
	}
	public static void ReloadScripts() {
		LuaController.luaScripts.Clear ();

		LuaFilesContainer lfc = Resources.Load<LuaFilesContainer> ("luascripts");
		for (int i = 0; i < lfc.pathname.Count; i++) {
			LuaController.luaScripts.Add(lfc.pathname[i], lfc.script[i]);
		}
		LoadAdditionalScripts ();
		Resources.UnloadAsset (lfc);
	}
	public static void LoadAdditionalScripts () {
		string datapath = Application.persistentDataPath;
	
		DirectoryInfo dir = new DirectoryInfo(datapath);

//		Debug.Log(Application.persistentDataPath);
		FileInfo[] info = dir.GetFiles("*.lua",SearchOption.AllDirectories);
		foreach (FileInfo f in info) {
			string namePath = f.FullName.Replace( dir.FullName + Path.DirectorySeparatorChar, "");
			namePath = namePath.Replace("\\", "/");
			namePath = namePath.Replace(".lua", "");
			byte [] filebytes = File.ReadAllBytes (f.FullName );
			string lua = Encoding.UTF8.GetString(filebytes, 0, filebytes.Length);
			if (!LuaController.luaScripts.ContainsKey(namePath)){
				LuaController.luaScripts.Add(namePath, lua);
			} else {
				LuaController.luaScripts[namePath] =  lua;
			}
		}
		
	}

	public static void Init () {
		lua = new LuaState();

		LuaController.ReloadScripts ();
		lua ["MainLuaController"] = LuaController.instance;
		lua ["luaScripts"] = LuaController.luaScripts;
		if (!string.IsNullOrEmpty( engineLoad)){
			LuaController.RequireLua (engineLoad );
		}
		if (!string.IsNullOrEmpty( requiredModules)){
			LuaController.RequireLua (requiredModules );
		}
		LuaController.toLuaObject = lua.GetFunction ("LuaObject");
		LuaController.inspectedDefGet = lua.GetFunction ("returnInspectedType");
	}

	public  void RestartLua () {
		if (luaStop != null){
			luaStop.Call();
		}
		Awake ();
	}

	public  void GetFunctions () {
		luaStart = lua.GetFunction ("StartAll");
		luaStop = lua.GetFunction ("StopAll");
		luaOnGui = lua.GetFunction ("LuaOnGui");
		luaUpdate = lua.GetFunction("UpdateAll");
		luaMouseDown = lua.GetFunction("MouseDown");
		luaMouseUp = lua.GetFunction("MouseUp");
		luaMouseMove = lua.GetFunction("MouseMove");
	}

	public static object [] RequireLua (string path ) {
		return lua.DoString (LuaController.luaScripts[ path], path, null );
	}

	void Update () {
		luaUpdate.Call(Time.deltaTime);
	}



	void OnGUI () {
		luaOnGui.Call();
		Event e = Event.current;
		if (e.isMouse ) {
			if (Input.GetMouseButtonDown(0)) {
				luaMouseDown.Call(0, e.mousePosition);
			} else if (Input.GetMouseButtonUp(0)){
				luaMouseUp.Call(0, e.mousePosition);
			} else if (Input.GetMouseButton(0)){
				luaMouseMove.Call(0, e.delta);
			}
			for (int i= 1; i < Input.touches.Length; i++) {
				if (Input.GetMouseButtonDown(i)) {
					luaMouseDown.Call(i, Input.touches[i].position);
				} else if (Input.GetMouseButtonUp(0)){
					luaMouseUp.Call(i, Input.touches[i].position);
				} else if (Input.GetMouseButton(0)){
					luaMouseMove.Call(i, Input.touches[i].deltaPosition);
				}
			}

		}


	}

	void OnApplicationQuit() {
		if (luaStop != null){
			luaStop.Call();
		}
		Debug.Log ("game quit " );
	}
}

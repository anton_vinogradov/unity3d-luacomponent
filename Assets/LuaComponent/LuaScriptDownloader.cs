﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Text.RegularExpressions;

public class LuaScriptDownloader : MonoBehaviour {

//	string ur = "https://bitbucket.org/anton_vinogradov/unity3d-luacomponent/raw/78a20a01ad7245edc468b2b4a5bdf45681835728/Assets/";

	void Start () {
//		string testurl = "https://bitbucket.org/anton_vinogradov/unity3d-luacomponent/raw/9a77b788b648d84e6a851b9dd820e20b258876bd/Assets/";
//		StartCoroutine(DownloadAndRequire ("LuaScripts/printtest",testurl ));
//		StartCoroutine( BitbucketProcess (bbr, "Assets/LuaScripts/Starter"));
	}

	public IEnumerator DownloadAndStore (string namePath, string folderurl) {
		string finalurl = folderurl + namePath + ".lua";
		WWW www = new WWW(finalurl);
		yield return www;
		if (string.IsNullOrEmpty(www.error) ){
			string lua = www.text;
			namePath = namePath.Replace("\\", "/" );
			if (!LuaController.luaScripts.ContainsKey(namePath)){
				LuaController.luaScripts.Add(namePath, lua);
			} else {
				LuaController.luaScripts[namePath] =  lua;
			}
			string fullpath = Path.Combine( Application.persistentDataPath, namePath + ".lua");
			fullpath = fullpath.Replace("/", "\\" );
			string dirPath = fullpath.Remove( fullpath.LastIndexOf("\\"));
//			Debug.Log(fullpath);
//			Debug.Log(dirPath);
			if (!Directory.Exists(dirPath)) {
				Directory.CreateDirectory(dirPath);
			}
			File.WriteAllText (fullpath, lua);
		}

	}

	public IEnumerator DownloadAndRequire(string namePath, string folderurl) {
		yield return StartCoroutine(DownloadAndStore(namePath, folderurl));
		LuaController.RequireLua (namePath);
	}

	public IEnumerator DownloadRequireAndDostring(string namePath, string folderurl, string lua) {
		yield return StartCoroutine(DownloadAndStore(namePath, folderurl));
		LuaController.RequireLua (namePath);
		if (!string.IsNullOrEmpty (lua)) {
			LuaController.lua.DoString (lua);
		}
	}

	IEnumerator BitbucketProcess (string repository, string filePath) {
		WWW www = new WWW(repository);
		yield return www;
		if (string.IsNullOrEmpty (www.error)) {
			string pagetext = www.text;
//			pagetext = "some text test ";
//			string matchexp = "data%-hash=(.-)";
			pagetext = pagetext.Replace("[[", " ");
			pagetext = pagetext.Replace("]]", " ");
			string luacode = "string.match([[" + pagetext + "]], 'data%-hash%s*=%s*\"(.-)\"')";
			Debug.Log(luacode);
			string hashcode = LuaController.lua.DoString(luacode)[0] as string;

//			Regex rx = new Regex(matchexp);
////
//			MatchCollection matches = rx.Matches(pagetext);
////
			Debug.Log(	hashcode);
		}
	}
	// Update is called once per frame
	void Update () {
	
	}
}

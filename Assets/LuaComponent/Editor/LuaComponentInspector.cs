﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.IO;
using System.Text;
using LuaInterface;


[CustomEditor(typeof(LuaComponent))]
public class LuaComponentInspector : Editor {
	LuaComponent Target;

//	Vector2 scroll1 = Vector2.zero;
//	Vector2 scroll2 = Vector2.zero;
//	bool fold1 = false;
//	bool fold2 = false;
	bool foldInspected = true;
	public static bool loaded = false;

	void OnEnable() {
		Target = (LuaComponent) target;

		if (!Application.isPlaying){
			LuaController.ReloadScripts();
			LuaController.Init ();
			Target.Init ();

		}
		LuaComponentInspector.loaded = true;
	}

	public override void OnInspectorGUI() {
		if (!LuaComponentInspector.loaded){
			Debug.Log("OnInspectorGUI");
			OnEnable() ;
		}
		bool changed = false;
		for (int i = 0; i < Target.luaList.Count; i++) {
			GUILayout.BeginHorizontal();
			GUILayout.Label(Target.luaList[i]);
			GUILayout.FlexibleSpace();
			if ( i > 0 && GUILayout.Button("-", GUILayout.Width(30)) ){
				Target.luaList.RemoveAt(i);
				changed = true;
				break;
			}
			GUILayout.EndHorizontal();
		}

		var classes = LuaController.lua.GetTable ("BehaviorsTable");
		List <string> newlist = new List<string> ();
		foreach (DictionaryEntry entry in classes) {
			var k = entry.Value as string;
			if (!Target.luaList.Contains(k)){
				newlist.Add(k);
			}
		}
		if (newlist.Count > 0){
			int curr = EditorGUILayout.Popup("+", -1, newlist.ToArray());
			if (curr >= 0){
				string newclass = newlist [curr];
				if (!Target.luaList.Contains(newclass)){
					Target.luaList.Add(newclass);

					changed = true;
				}
			}
		}
		if (changed){
			for (int i = 0; i < Target.luaList.Count; i++) {
				LuaController.lua.GetFunction("CheckDependencies").Call(Target.luaList[i], 	Target.luaList);
			}
			OnEnable() ;
			return;
		}

//		GUILayout.Label (Target.mainName );
//		
		EditorGUI.BeginDisabledGroup (Application.isPlaying);
//		if (GUILayout.Button("Open for edit")){
//			string openpath =  Application.dataPath + Target.mainName + ".lua";
//			Debug.Log(openpath);
//			System.Diagnostics.Process.Start(openpath);
//		}
		if (Target.onInspectorGUI != null ){
			foldInspected = EditorGUILayout.Foldout (foldInspected, "Inspected Fields");
			if (foldInspected){
				Target.onInspectorGUI.Call(Target.mluaObject);
			}
			if (GUI.changed){
				Target.Validate();
			}

		}
		EditorGUI.EndDisabledGroup ();
//		GUILayout.EndVertical ();
	}
	

}

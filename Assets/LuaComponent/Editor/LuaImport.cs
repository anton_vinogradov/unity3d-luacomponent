﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

public class LuaImport : AssetPostprocessor 
{
	public static void OnPostprocessAllAssets (
		string[] importedAssets,
		string[] deletedAssets,
		string[] movedAssets,
		string[] movedFromAssetPaths ){

		foreach (string asset in deletedAssets) {
			if (hasLuaExtention( asset)){

					DeleteLua(asset);

			}
		}

		foreach (string asset in importedAssets) {
			if (hasLuaExtention( asset)){

					StoreLua(asset);

			}
		}
		for (int i= 0; i < movedFromAssetPaths.Length; i++) {
			string asset = movedFromAssetPaths[i];
			if (hasLuaExtention( asset)){
				MoveLua(asset, movedAssets[i]);
			}
		}
	}

	static public void MoveLua(string oldasset,string newasset){
		string oldstorepath = oldasset.Replace (".lua", "");
		oldstorepath = oldstorepath.Replace ("Assets/", "");
		string newstorepath = newasset.Replace (".lua", "");
		newstorepath = newstorepath.Replace ("Assets/", "");

		LuaFilesContainer lfiles = ( LuaFilesContainer) AssetDatabase.LoadAssetAtPath("Assets/LuaComponent/Resources/luascripts.asset", typeof(LuaFilesContainer) );
		if (lfiles == null){
			lfiles = ScriptableObject.CreateInstance<LuaFilesContainer> ();
			lfiles.pathname = new List<string>();
			lfiles.script = new List<string>();
			AssetDatabase.CreateAsset(lfiles, "Assets/LuaComponent/Resources/luascripts.asset");
			Debug.Log("no lfc");
		}
		int oldid = lfiles.pathname.IndexOf (oldstorepath);
		lfiles.pathname [oldid] = newstorepath;
		EditorUtility.SetDirty (lfiles);

		AssetDatabase.SaveAssets ();
		AssetDatabase.Refresh ();
		LuaController.ReloadScripts();
		LuaController.Init ();
		LuaComponentInspector.loaded = false;
	}

	static public void DeleteLua(string asset){
		LuaFilesContainer lfiles = ( LuaFilesContainer) AssetDatabase.LoadAssetAtPath("Assets/LuaComponent/Resources/luascripts.asset", typeof(LuaFilesContainer) );
		if (lfiles == null){
			lfiles = ScriptableObject.CreateInstance<LuaFilesContainer> ();
			lfiles.pathname = new List<string>();
			lfiles.script = new List<string>();
			AssetDatabase.CreateAsset(lfiles, "Assets/LuaComponent/Resources/luascripts.asset");
			Debug.Log("no lfc");
		}
		string storepath = asset.Replace (".lua", "");
		storepath = storepath.Replace ("Assets/", "");
		int oldid = lfiles.pathname.IndexOf (storepath);
		lfiles.pathname.RemoveAt (oldid);
		lfiles.script.RemoveAt (oldid);
		EditorUtility.SetDirty (lfiles);

		AssetDatabase.SaveAssets ();
		AssetDatabase.Refresh ();
		LuaController.ReloadScripts();
		LuaController.Init ();
		LuaComponentInspector.loaded = false;

	}

	static public void StoreLua(string asset){

		byte[] filebytes = readBytes(asset);
		string lua = Encoding.UTF8.GetString(filebytes, 0, filebytes.Length);
		string storepath = asset.Replace (".lua", "");
		storepath = storepath.Replace ("Assets/", "");

		LuaFilesContainer lfiles = ( LuaFilesContainer) AssetDatabase.LoadAssetAtPath("Assets/LuaComponent/Resources/luascripts.asset", typeof(LuaFilesContainer) );
		if (lfiles == null){
			lfiles = ScriptableObject.CreateInstance<LuaFilesContainer> ();
			lfiles.pathname = new List<string>();
			lfiles.script = new List<string>();
			AssetDatabase.CreateAsset(lfiles, "Assets/LuaComponent/Resources/luascripts.asset");
			Debug.Log("no lfc");
		}

		if (lfiles.pathname.Contains (storepath)) {
			int oldid = lfiles.pathname.IndexOf (storepath);
			lfiles.pathname [oldid] = storepath;
			lfiles.script [oldid] = lua;
		} else {
			lfiles.pathname.Add (storepath);
			lfiles.script.Add (lua);
		}

		EditorUtility.SetDirty (lfiles);

		AssetDatabase.SaveAssets ();
		AssetDatabase.Refresh ();
		LuaController.ReloadScripts();
		LuaController.Init ();
		LuaComponentInspector.loaded = false;
	}



	static byte[] readBytes(string aPath)
	{
		if (!File.Exists(aPath ))
		return new byte[] {0};
	
		return File.ReadAllBytes (aPath );
	}
	private static bool hasLuaExtention(string asset)
	{
		return asset.EndsWith(".lua");
	}


}

using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using LuaInterface;

public class DBEditor : EditorWindow {
	public static LuaTable    luaDBEditor;
	public static LuaFunction luaInit;
	public static LuaFunction luaOnGUI;

	[MenuItem ("Game/DB Editor")]
	public static void Init(){
		DBEditor window = (DBEditor)EditorWindow.GetWindow (typeof (DBEditor));
		window.title = "DB Editor";
	}

	void OnFocus(){
		if (LuaComponentInspector.loaded == false){
			Reload();
			LuaComponentInspector.loaded = true;
		}
	}

	void Reload(){
		LuaController.ReloadScripts();
		LuaController.Init ();

		luaDBEditor = LuaController.lua.GetTable("DBEditor");
		if (luaDBEditor != null){
			luaInit  = luaDBEditor["Init"] as LuaFunction;
			luaOnGUI = luaDBEditor["OnGUI"] as LuaFunction;
			luaInit.Call(luaDBEditor);
		}
	}

	void OnGUI(){
		if (luaOnGUI != null) {
			luaOnGUI.Call(luaDBEditor);
		} else {
			Reload();
		}
	}
}

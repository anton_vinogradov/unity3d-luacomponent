﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.IO;
using System.Text;
using LuaInterface;


[CustomEditor(typeof(LuaController))]
public class LuaControllerInspector : Editor {
	LuaController Target;
	List<string> names ;

	void OnEnable() {
		Target = (LuaController) target;
		LuaController.ReloadScripts ();
	}



	public override void OnInspectorGUI() {
		if (GUILayout.Button("Reload scripts")){
			LuaController.ReloadScripts();
		}
		names =  new List<string>( LuaController.luaScripts.Keys);
	
		LuaController.engineLoad = ScriptSelect ("Engine script", LuaController.engineLoad);
		LuaController.requiredModules = ScriptSelect ("Requiring scripts", LuaController.requiredModules);
		LuaController.starter = ScriptSelect ("Starter script", LuaController.starter);
		LuaController.dbfile = ScriptSelect ("DataBase file", LuaController.dbfile);
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		GUILayout.Label ("All Scripts");
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();

		for (int i = 0; i < names.Count; i++) {
			GUILayout.BeginHorizontal();

			var style = new GUIStyle("Box");

			EditorGUILayout.SelectableLabel(names[i],style, GUILayout.Height(20));
			GUILayout.FlexibleSpace();
			if (GUILayout.Button("Open for edit", GUILayout.Width(100))){
				string openpath =  Application.persistentDataPath +"/"+ names[i] + ".lua";
				if (!File.Exists(openpath)){
					openpath = Application.dataPath +"/"+ names[i] + ".lua";
					Debug.Log(openpath);
				}
				System.Diagnostics.Process.Start(openpath);
			}
			GUILayout.EndHorizontal();
		}

	}
	string ScriptSelect(string label ,string scriptName ) {
		int curr = names.IndexOf (scriptName);
		GUILayout.Label (label);
		curr = EditorGUILayout.Popup (curr, names.ToArray ());
		if (names.Count > 0 && curr >= 0){
			return names [curr];
		}
		return scriptName;
	}

}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LuaInterface;

[System.Serializable]
public class SerializableLuaField {

	[SerializeField]
	public string key = "";

	[SerializeField]
	public string luaType = "";

	[SerializeField]
	public string data = "";

	[SerializeField]
	public string systemType = "";

	public  SerializableLuaField (string k, string lt, string st ) {
		key = k;
		luaType = lt;
		systemType = st;
	}
}

public class LuaComponent : LuaObject, ISerializationCallbackReceiver {
	[System.NonSerialized]
		public LuaFunction start;
	[System.NonSerialized]
		public LuaFunction destroy;
	[System.NonSerialized]
		public LuaFunction disable;
	[System.NonSerialized]
		public LuaFunction enable;
	[System.NonSerialized]
		public LuaFunction validate;
	[System.NonSerialized]
		public LuaFunction require;
	[System.NonSerialized]
		public LuaFunction onInspectorGUI;
	[System.NonSerialized]
	 public LuaTable mluaObject ;

	public LuaTable luaObject {
		get{
			if (mluaObject == null){
				Init();
			}
			return mluaObject;
		}
	}

//	public string mainName = "BaseClass";
	public List <string> luaList = new List<string> (){"BaseClass"};

	[SerializeField]
	public List <SerializableLuaField> serializedData = new List<SerializableLuaField>();

	[System.NonSerialized]
	public Dictionary<string, object> serializedValues = new Dictionary<string, object>();



	public void OnBeforeSerialize()
	{

//		print ("OnBeforeSerialize");
//		serializedData.Clear ();

		for (int i = 0; i < serializedData.Count; i++) {
			string key = serializedData[i].key;
			if (!serializedValues.ContainsKey(key)){ Debug.Log ("try ser " + key + "name " + name);}
			object val = serializedValues[key];
			serializedData[i].data = LuaController.Serialize(val);

		}


	}
	public void OnAfterDeserialize()
	{
//		print ("OnAfterDeserialize " + serializedValues.Count);
		serializedValues.Clear ();
		for (int i = 0; i < serializedData.Count; i++) {
			SerializableLuaField f = serializedData[i];
			string key = f.key;
			object val = LuaController.Deserialize  (f.data, f.systemType);
			serializedValues.Add(key,val);
//			if (serializedData[i].luaType.Contains("Vector")) print (f.data + " = "+ val);
		}

	}

	bool inited = false;

//	public object this [string key] {
//		get {
//			if (!serializedValues.ContainsKey(key)) print ("try get " + key);
//			return serializedValues[key];
//		}
//		set {
//			if (!serializedValues.ContainsKey(key)) print ("try set " + key);
//			serializedValues[key] = value;
//		}
//	}


	public static LuaTable Create(List <string> classes) {
		GameObject go = new GameObject ("LuaObject");
		LuaComponent component = go.AddComponent<LuaComponent>();
		component.luaList = classes;
		component.inited = false;
		component.Init();
		return component.luaObject;
	}

	public static LuaTable Copy(GameObject obj) {
		GameObject go = GameObject.Instantiate(obj) as GameObject;
		LuaComponent component = go.GetComponent<LuaComponent>();
//		print ("Copy" + go.name);
		component.inited = false;
		return component.luaObject;
	}


	public void SetSerialized(string key, object  value, string type) {
		serializedValues [key] = value;

	}

	public void EditorInit() {
//		print ("EditorInit");
		var insp = mluaObject["inspectable"] as LuaTable;
#if UNITY_EDITOR
		List <string> oldkeys = new List <string> ();
		for (int i = 0; i < serializedData.Count; i++) {
			oldkeys.Add(serializedData[i].key);
		}
#endif
		foreach(DictionaryEntry entry in insp) {
			var key = entry.Key as string;
			if (!serializedValues.ContainsKey(key)){
				string type = entry.Value as string;
				if (type == null){
					var t = entry.Value as LuaTable;
					type = t[1] as string;
				}
				if (type == "GameObject"){
					GameObject defaultval = null;
					serializedData.Add(new SerializableLuaField(key,type,typeof(GameObject).AssemblyQualifiedName ));
					serializedValues.Add(key,defaultval);
				} else {
					object defaultval = LuaController.InspectedInit(type);
					serializedData.Add(new SerializableLuaField(key,type,defaultval.GetType().AssemblyQualifiedName ));
					serializedValues.Add(key,defaultval);
				}


			}
#if UNITY_EDITOR
			oldkeys.Remove(key);
#endif
		}
#if UNITY_EDITOR
		while (oldkeys.Count > 0) {
			var key = oldkeys[0];
			oldkeys.RemoveAt(0);
			for (int i = 0; i < serializedData.Count; i++) {
				if (serializedData[i].key == key){
					serializedData.RemoveAt(i);
					serializedValues.Remove(key);
					break;
				}
			}
		}
		onInspectorGUI = mluaObject["OnInspectorGUI"] as LuaFunction;
#endif

	}

	public void Init() {
		if (inited && Application.isPlaying){
			return;
		}
		inited = true;

		LuaFunction luaclass = LuaController.toLuaObject;
		luaclass.Call(luaList, this);

		EditorInit ();

		start   = mluaObject["OnStart"]   as LuaFunction;
		destroy = mluaObject["OnDestroy"] as LuaFunction;
		disable = mluaObject["OnDisable"] as LuaFunction;
		enable  = mluaObject["OnEnable"]  as LuaFunction;
		require = mluaObject["OnRequire"] as LuaFunction;

		require.Call(mluaObject);

#if UNITY_EDITOR
		if (!Application.isPlaying){
			validate = mluaObject["OnValidate"] as LuaFunction;
			Validate ();
		}
#endif
	}


	void Reset() {
		inited = false;
		Init();
	}

	public void Awake() {
		useGUILayout = false;
		Init();
	}

	public void Start() {
		if (start != null) {
			start.Call(mluaObject);
		}
	}

	void OnDestroy() {
		if (destroy != null) {
			destroy.Call(mluaObject);
		}
	}

	void OnEnable() {
		if (enable != null) {
			enable.Call(mluaObject);
		}
	}

	void OnDisable() {
		if (disable != null) {
			disable.Call(mluaObject);
		}
	}

	public Component Required(string component) {
		Component c = GetComponent (component);
		if (c == null ){
			return this.gameObject.AddComponent ( GetType(component) );
		}
		return c;
	}

	public void Validate() {
		if (validate != null){
			validate.Call(mluaObject);
		}
	}


	public static object GetListOfT(System.Type T)
	{
		return System.Activator.CreateInstance(typeof(List<>).MakeGenericType(T));

	}


	public static System.Type GetType(string typeName) {
		var type = System.Type.GetType(typeName);
		if (type != null) return type;
		foreach (var a in System.AppDomain.CurrentDomain.GetAssemblies())
		{
			type = a.GetType(typeName);
			if (type != null)
				return type;
		}
		return null ;
	}

}

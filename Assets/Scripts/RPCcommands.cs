﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LuaInterface;

public class RPCcommands : MonoBehaviour {

	public Dictionary <string, string []> dataList = new Dictionary <string, string []> ();
	public Dictionary <NetworkPlayer, List<string> > sendingList = new Dictionary <NetworkPlayer, List<string> > ();
	bool datatransfering = false;

	NetworkView networkView;
	LuaScriptDownloader lsd;
	
	public  NetworkPlayer serverPl = new NetworkPlayer();
	public  NetworkPlayer selfPl;

	LuaFunction luaToStr;
	LuaFunction callbackReciever;
	string debugstr = "";
	void Start () {
		lsd = GetComponent<LuaScriptDownloader> ();
		networkView = GetComponent<NetworkView> ();
		luaToStr = LuaController.lua.GetFunction ("luaToStr");
		callbackReciever = LuaController.lua.GetFunction ("callbackReciever");
		selfPl = Network.player;
	}
	public void netRPC (string func, NetworkPlayer player, LuaTable luaargs) {
		int count = luaargs.Values.Count;
		object [] args = new object[count];
		for (int i=0; i < count ; i++) {
			args[i] = luaargs[i+1];
		}
		networkView.RPC(func, player, args);
	}

	[RPC]
	public  void DoLuaCode(NetworkPlayer player, string lua, string callbackid) {
		if (!string.IsNullOrEmpty(lua)){
			object[] result = LuaController.lua.DoString(lua);
			if (!string.IsNullOrEmpty(callbackid) && result.Length > 0 && result[0] != null ){
				string answer = string.Empty;
				for (int i=0; i < result.Length ; i++) {
					answer += luaToStr.Call( result[i])[0] as string;
			
				}

				networkView.RPC("ResultCallback", player, callbackid, answer);
			}
		}

	}

	[RPC]
	public  void ResultCallback(string callbackid, string lua) {
		if (!string.IsNullOrEmpty(lua)){
			callbackReciever.Call(callbackid, lua);
		}
		
	}

	[RPC]
	public  void DownloadRequireAndDo( string namePath, string url, string lua) {
		lsd.StartCoroutine(lsd.DownloadRequireAndDostring(namePath, url, lua));
	}
	
//	[RPC]
//	public  void GetLuaScript(NetworkPlayer player, string name) {
//		string script = LuaShell.Instance.luaScripts [name];
//		StartCoroutine(SendDataTo (player, "ScriptReceived", script));
//	}

	
	[RPC]
	public  void ServerPlayer(NetworkPlayer player) {
		serverPl = player;
		Debug.Log ("ServerPlayer: " +player);
	}
	
	void OnPlayerConnected(NetworkPlayer player) {
		sendingList.Add (player, new  List<string> ());
		networkView.RPC("ServerPlayer", player,Network.player );
		Debug.Log ("RPCFuncs OnPlayerConnected: " +player);
	}
	void OnPlayerDisconnected(NetworkPlayer player) {
		sendingList.Remove (player);
		Debug.Log ("RPCFuncs OnPlayerDisconnected: " +player);
	}
	
	void OnConnectedToServer() {
		selfPl = Network.player;
		sendingList.Add (serverPl, new  List<string> ());
		Debug.Log ("RPCFuncs OnConnectedToServer: " +Network.player.guid);
	}
	void OnDisconnectedFromServer(NetworkDisconnection info) {
		if (Network.isClient) {
			sendingList.Remove (serverPl);
			Debug.Log ("RPCFuncs OnConnectedToServer: " + Network.player.guid);
		}
	}
	
//	public IEnumerator SendDataTo(NetworkPlayer player, string func, string data) {
//		if (!sendingList [player].Contains (func)) {
//			sendingList [player].Add (func);
//			int size = Mathf.CeilToInt( data.Length  / 4095f);
//			for (int i=0; i < size; i++) {
//				string s = data.Substring(i*4095, Mathf.Min(data.Length - i*4095,  4095));
//				networkView.RPC("SendData", player, s, i, size, func);
//				yield return new WaitForSeconds(0.01f);
//			}
//			for (int i=0; i < sendingList.Count; i++) {
//				if (sendingList [player][i] == func){
//					sendingList[player].RemoveAt (i);
//					break;
//				}
//			}
//			
//		} else {
//			
//		}
//		
//	}
//	
//	[RPC]
//	public  void SendData(string data, int id, int size, string func) {
//		if (!dataList.ContainsKey(func)){
//			dataList.Add(func, new string [size]);
//		}
//		
//		dataList[func][id] = data;
//		
//		bool complete = true;
//		for (int i=0; i < size; i++) {
//			if (string.IsNullOrEmpty(dataList[func][i])){
//				complete = false;
//				break;
//			}
//		}
//		if (complete){
//			string s  = System.String.Join("", dataList[func]);
//			dataList.Remove(func);
//			this.gameObject.SendMessage(func, s);
//		}
//		
//		Debug.Log ("id: " +id + " | size: " +size +"|" );
//	}

	
//	void OnGUI () {
//		GUILayout.Label (debugstr);
//	}
}

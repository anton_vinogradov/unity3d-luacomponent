﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Runtime.InteropServices;

public class NetCode : MonoBehaviour
{
	const int MAX_PACKET_LENGTH = 1024;
	
	public string terminalScriptName = "";
	
	string mTerminalRPCName = "";
	NetCode mTerminalScript;
	NetworkView networkView;

	void Awake () {
		networkView = GetComponent<NetworkView> ();
	}

	void init()
	{
		mTerminalScript = (NetCode)GetComponent(terminalScriptName);
		
		mTerminalRPCName = terminalScriptName + "RPC";
	}
	
	protected void doRPC(string name, NetworkPlayer player, params object[] args)
	{
		if (mTerminalScript == null)
		{
			init();
		}
		
		networkView.RPC(mTerminalRPCName, player, name, getRPCBytes(name, args));
	}
	
	protected void doRPC(string name, RPCMode mode, params object[] args)
	{
		if (mTerminalScript == null)
		{
			init();
		}
		
		networkView.RPC(mTerminalRPCName, mode, name, getRPCBytes(name, args));
	}
	
	public byte[] getRPCBytes(string RPCname, params object[] objects)
	{
		MethodInfo tempMethod = mTerminalScript.GetType().GetMethod(RPCname, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
		
		if (tempMethod == null)
		{
			Debug.Log("RPC function does not exist: '" + RPCname + "' in class " + mTerminalScript.GetType());
			return null;
		}
		
		ParameterInfo[] tempInfo = tempMethod.GetParameters();
		
		if (objects.Length != tempInfo.Length)
		{
			Debug.Log("RPC function '" + RPCname + "' parameter counts don't match. Expected " + tempInfo.Length + " got " + objects.Length);
			return null;
		}
		
		List<byte[]> tempBytes = new List<byte[]>();
		
		for (int i = 0; i < tempInfo.Length; i++)
		{
			if (tempInfo[i].ParameterType != objects[i].GetType())
			{
				Debug.Log("RPC function '" + RPCname + "' parameter " + (i + 1) + " is the wrong type. Expected " + tempInfo[i].ParameterType + " got " + objects[i].GetType());
				return null;
			}
			
			tempBytes.Add(ToByteArray(objects[i], MAX_PACKET_LENGTH));
		}
		
		int totalSize = 0;
		
		for (int i = 0; i < tempBytes.Count; i++)
		{
			totalSize += tempBytes[i].Length;
		}
		
		if (totalSize > 0)
		{
			byte[] finalBytes = new byte[totalSize];
			int currentOffset = 0;
			
			for (int i = 0; i < tempBytes.Count; i++)
			{
				System.Array.Copy(tempBytes[i], 0, finalBytes, currentOffset, tempBytes[i].Length);
				
				currentOffset += tempBytes[i].Length;
			}
			
			return finalBytes;
		}
		else
		{
			byte[] finalBytes = new byte[1];
			finalBytes[0] = 0;
			
			return finalBytes;
		}
	}
	
	
	
	
	protected void dynamicRPC(string name, byte[] data)
	{
		int size;
		object tempObject = null;
		int currentOffset = 0;
		
		if (!FromByteArray(typeof(string), data, 0, out size, ref tempObject))
		{
			Debug.Log("Malformed RPC call");
			return;
		}
		
		string RPCname = name;
		MethodInfo tempMethod = this.GetType().GetMethod(RPCname, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
		
		if (tempMethod == null)
		{
			Debug.Log("RPC function does not exist: '" + name + "' in class " + GetType());
			return;
		}
		
		ParameterInfo[] tempInfo = tempMethod.GetParameters();
		
		object[] parameters = new object[tempInfo.Length];
		
		for (int i = 0; i < tempInfo.Length; i++)
		{
			if (FromByteArray(tempInfo[i].ParameterType, data, currentOffset, out size, ref tempObject))
			{
				parameters[i] = tempObject;
			}
			else
			{
				Debug.Log("RPC function '" + name + "' was unable to convert parameter: " + (i + 1));
				return;
			}
			
			currentOffset += size;
		}
		
		tempMethod.Invoke(this, parameters);
	}
	
	protected bool FromByteArray(Type type, byte[] rawValue, int index, out int size, ref object data)
	{
		//Pack length info in the first 4 bytes
		if (type == typeof(byte[]))
		{
			size = BitConverter.ToInt32(rawValue, index);
			
			byte[] tempData = new byte[size];
			
			Array.Copy(rawValue, index + 4, tempData, 0, size);
			
			size += 4;
			
			data = (object)tempData;
			
			return true;
		}
		//Strings need to be handled seperately
		else if (type == typeof(string))
		{
			for (int i = index; i < rawValue.Length; i++)
			{
				if (rawValue[i] == 0)
				{
					//Found end of string
					
					size = i - index + 1;
					
					data = (object)Encoding.ASCII.GetString(rawValue, index, i - index);
					
					return true;
				}
			}
			
			size = 0;
			
			return false;
		}
		else
		{
			size = Marshal.SizeOf(type);
			
			if (rawValue.Length >= index + size)
			{
				byte[] tempBytes = new byte[size];
				Array.Copy(rawValue, index, tempBytes, 0, size);
				
				GCHandle handle = GCHandle.Alloc(tempBytes, GCHandleType.Pinned);
				data = (object)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), type);
				handle.Free();
				
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	
	protected byte[] ToByteArray(object value, int maxLength)
	{
		//Pack length info in the first 4 bytes
		if (value.GetType() == typeof(byte[]))
		{
			int tempLength = ((byte[])value).Length;
			byte[] tempByteArray = new byte[tempLength + 4];
			
			byte[] lengthBytes = BitConverter.GetBytes((Int32)tempLength);
			
			Array.Copy(lengthBytes, 0, tempByteArray, 0, 4);
			Array.Copy((byte[])value, 0, tempByteArray, 4, tempLength);
			
			return tempByteArray;
		}
		//Strings need to be handled seperately
		else if (value.GetType() == typeof(string))
		{
			byte[] rawdata = Encoding.ASCII.GetBytes((string)value + "\0");
			
			return rawdata;
		}
		else
		{
			int rawsize = Marshal.SizeOf(value);
			byte[] rawdata = new byte[rawsize];
			GCHandle handle =
				GCHandle.Alloc(rawdata,
				               GCHandleType.Pinned);
			Marshal.StructureToPtr(value,
			                       handle.AddrOfPinnedObject(),
			                       false);
			handle.Free();
			if (maxLength < rawdata.Length)
			{
				byte[] temp = new byte[maxLength];
				Array.Copy(rawdata, temp, maxLength);
				return temp;
			}
			else
			{
				return rawdata;
			}
		}
	}
}
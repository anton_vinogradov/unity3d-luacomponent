RegisterClass 'BaseClass'

function BaseClass:Init()
	self.serialized = self.base.serializedValues

	self.gameObject = self.base.gameObject
	self.transform  = self.mTransform
	self.name       = self.base.name

	self.listeners = {}
	self.flaglisteners = {}
end

function BaseClass:OnRequire()

end

function BaseClass:subscribe(eventName, listener)
	if eventName:find('.', 1, true) then
		if not self.flaglisteners[eventName] then
			self.flaglisteners[eventName] = {}
		end
		self.flaglisteners[eventName][listener] = listener
		addTableListener(game.mSave, eventName, listener)
	else
		if not self.listeners[eventName] then
			self.listeners[eventName] = {}
		end
		self.listeners[eventName][listener] = listener
		LuaManager:addListener(eventName, listener)
	end
end

function BaseClass:notify(eventName, params)
	LuaManager:notifyObject(eventName, self, params)
end

function BaseClass:unsubscribe(eventName, func)
	if eventName:find('.', 1, true) then
		if self.flaglisteners[eventName] then
			if not func then
				for func in pairs(self.flaglisteners[eventName]) do
					delTableListener(game.mSave, eventName, func)
				end
				self.flaglisteners[eventName] = nil
			else
				delTableListener(game.mSave, eventName, func)
				self.flaglisteners[eventName][func] = nil
			end
		end
	else
		if self.listeners[eventName] then
			if not func then
				for func in pairs(self.listeners[eventName]) do
					LuaManager:delListener(eventName, func)
				end
				self.listeners[eventName] = nil
			else
				LuaManager:delListener(eventName, func)
				self.listeners[eventName][func] = nil
			end
		end
	end
end

function BaseClass:__eq(b)
	return self.base:Equals(b.base)
end

function BaseClass:__gc()
	if self.__actions ~= nil then
		for i = 1, #self.__actions do
			self.__actions[i]:stop()
		end
	end
end

function BaseClass:OnDestroy()
	self:__gc()
	for k in pairs(self.listeners) do
		self:unsubscribe(k)
	end
	for k in pairs(self.flaglisteners) do
		self:unsubscribe(k)
	end

end

function BaseClass:OnValidate() end


function BaseClass:OnUpdate(aDelta)
	if self.mUpdate then
		self:mUpdate(aDelta)
	end
end


function BaseClass:SetActive(aActive)
	self.mObj:SetActive(aActive)
end

function BaseClass:SetPosition(aVector)
	-- self.mTransform.localPosition = Vector3(aVector.x, aVector.y, 0) -- aVector:getVector3()
end

function BaseClass:GetPosition()
	-- return Vec2.toVec2(self.mTransform.localPosition)
end

function BaseClass:SetGlobalPosition(aVector)
	-- local parent = self.mTransform.parent or self.mTransform
 --    local thePosition = parent:InverseTransformPoint(aVector:getVector3());
 --    self.mTransform.localPosition = thePosition
end

function BaseClass:GetGlobalPosition()
 --    local parent = self.mTransform.parent or self.mTransform
 --    local thePosition = parent:TransformPoint(self.mObj.transform.localPosition);
	-- return Vec2.toVec2(thePosition)
end

function BaseClass:SetScale(aVector)
	-- self.mTransform.localScale = aVector:getVector3()
end

function BaseClass:GetScale()
	-- return Vec2.toVec2(self.mTransform.localScale)
end

function BaseClass:SetRotation(aAngle)
	-- self.mTransform.eulerAngles = Vector3(0, 0, aAngle)
end

function BaseClass:GetRotation()
    -- if self.mTransform.eulerAngles == nil or self.mTransform.eulerAngles == Vector3.zero then
    --     return 0
    -- else
    --     return self.mTransform.eulerAngles.z
    -- end
end


function BaseClass:ResetTransforms()
	self.mTransform.localPosition = Vector3.zero
	self.mTransform.localScale = Vector3.one
	self.mTransform.localEulerAngles = Vector3.zero
end


function BaseClass:FindChild(aName)
	return self.mTransform:Find(aName)
end

function BaseClass:GetParent()
	return self.mTransform.parent
end


function BaseClass:SetParent(aParent, bool)
	self.transform:SetParent(aParent, bool or false)
end

function BaseClass:RemoveFromParent()
	self.mTransform.parent = nil
end

function BaseClass:RemoveChild(aObject)
	-- if aObject.mTransform:IsChildOf(self.mTransform) then
	-- 	aObject.mTransform.parent = nil
	-- end
end

function BaseClass:Destroy()
	if not IsNull(self.base) and not IsNull(self.base.gameObject) then
		GameObject.DestroyImmediate(self.base.gameObject)
	end
end

function BaseClass:DestroyChildren()
	local ch = self:getChildrenTransform()
	for i = 1, #ch do
		GameObject.DestroyImmediate(ch[i].gameObject)
	end
end

function BaseClass:GetChildrenTransform()
	local t = {}
	for i = 0, self.mTransform.childCount - 1 do
		local obj = self.mTransform:GetChild(i)
		t[#t + 1] = obj
	end
	return t
end

function BaseClass:GetAllChildren()
	-- local t = getAllChildren(self.mTransform)
	-- for i = 1, #t do
		-- t[i] = Object(t[i].gameObject)
	-- end
	-- return t
end

function BaseClass:AddComponent(aComponentName)
	return self.gameObject:AddComponent(aComponentName)
end

function BaseClass:GetComponent(aComponentName)
	return self.gameObject:GetComponent(aComponentName)
end

function BaseClass:RemoveComponent(aComponentName)
	UComponent.DestroyImmediate(self.gameObject:GetComponent(aComponentName))
end


-----------------------------------------------------------
--	LuaInspector
-----------------------------------------------------------
function BaseClass:OnInspectorGUI()
	GUILayout.BeginVertical({})

	for k, v in pairs (self.inspectable) do
		local ftype
		if type(v) == 'table' then
			ftype = v[1]
		else
			ftype = v
		end

		GUILayout.BeginHorizontal({})
			local func = inspectedTypes[ftype]
			if func then
				self.serialized[k] = func(k, self.serialized[k], self, v)
			end
		GUILayout.EndHorizontal()
	end

	GUILayout.EndVertical()
end

return BaseClass

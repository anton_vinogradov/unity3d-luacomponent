function isTable(v)
	return type(v) == "table"
end

function table.getn(aTable)
	local count = 0
	for _ in pairs(aTable) do
		count = count + 1
	end
	return count
end

function table.first(aTable)
	local _, v = next(aTable)
	return v
end

function table.copy(aTable)
	local t = {}
	for k, v in pairs(aTable) do
		t[k] = v
	end
	return t
end

function table.fill(aTable, aFrom, aTo, aValue)
	for i = aFrom, aTo do
		aTable[i] = aValue
	end
end

function table.add(aA, aB)
	local t = {}
	for k, v in pairs(aA) do
		t[k] = v
	end
	for k, v in pairs(aB) do
		t[k] = v
	end
	return t
end

function table.clean(aTable)
	for k in pairs(aTable) do
		aTable[k] = nil
	end
end

function table.reverse(aTable, aFrom, aTo)
	aFrom = aFrom or 1
	aTo = aTo or #aTable

	while aFrom < aTo do
		aTable[aFrom], aTable[aTo] = aTable[aTo], aTable[aFrom]
		aFrom = aFrom + 1
		aTo = aTo - 1
	end
end

function table.rotate(aTable, aStep)
	aStep = aStep % #aTable

	table.reverse(aTable, 1, aStep)
	table.reverse(aTable, aStep + 1, #aTable)
	table.reverse(aTable, 1, #aTable)
end

function table.append(aTableA, aTableB)
	assert(aTableA ~= aTableB, 'cannot append table to itself')

	local i = 1
	for k,v in pairs(aTableB) do
		if k == i then
			i = i + 1
			aTableA[#aTableA+1] = v
		else
			aTableA[k] = v
		end
	end
end

function table.swapItems(aTable, aKeyA, aKeyB)
	aTable[aKeyA], aTable[aKeyB] = aTable[aKeyB], aTable[aKeyA]
end

function table.combine(aKeys, aValues)
	local t = {}
	for i = 1, math.min(#aKeys, #aValues) do
		t[aKeys[i]] = aValues[i]
	end
	return t
end

function table.find(aTable, aItem)
	for k, v in pairs(aTable) do
		if v == aItem then
			return k
		end
	end
	return nil
end

function table.findAndRemove(aTable, aItem)
	for i = 1, #aTable do
		if aTable[i] == aItem then
			table.remove(aTable, i)
			return true
		end
	end
	return false
end

function table.compare(a,b)
	if #a ~= #b then return false end

	for i = 1, #a do
		if not a[i] == b[i] then
			return false
		end
	end

	return true
end

function table.keys(aTable)
	local keys = {}
	for k,_ in pairs(aTable) do
		keys[#keys + 1] = k
	end
	return keys
end

function table.any(aTable)
	local keys = table.keys(aTable)
	local rnd = math.floor(math.random(1, #keys))
	return aTable[keys[rnd]]
end

function table.merge(a, b)
	for k, v in pairs(b) do
		if isTable(v) and isTable(a[k]) then
			table.merge(a[k], v)
		else
			if not rawequal(v,v) then
				a[k] = nil
			else
				a[k] = v
			end
		end
	end
	return a
end

function table.diff(a,b)
	local d = {}
	local c = 0

	for k,v in pairs(b) do
		if isTable(v) and isTable(a[k]) then
			d[k] = table.diff(a[k], v)
			if d[k] then
				c = c + 1
			end
		else
			if a[k] ~= v then
				d[k] = v
				c = c + 1
			end
		end
	end

	for k in pairs(a) do
		if b[k] == nil then
			d[k] = 0/0
			c = c + 1
		end
	end

	if c > 0 then
		return d
	end
end

function table.binary(a,b,f)
	local d = {}
	local c = 0
	for k,v in pairs(b) do
		if isTable(v) and isTable(a[k]) then
			d[k] = table.binary(a[k], v, f)
			if d[k] then
				c = c + 1
			end
		else
			if f(a[k], v) then
				d[k] = v
				c = c + 1
			end
		end
	end
	if c > 0 then
		return d
	end
end

function table.filter(aTable, aFunc)
	local t = {}
	for k,v in pairs(aTable) do
		if aFunc(k,v) then
			t[k] = v
		end
	end
	return t
end

function table.filter_s(aTable, aCondition)
	local code = [[
		local src = select(1, ...)
		local dst = {}
		for k,v in pairs(src) do
			if ]] .. aCondition .. [[ then
				dst[k] = v
			end
		end
		return dst
	]]

	return load(code)(aTable)
end

function table.filter_arr(aTable, aCondition)
	local code = [[
		local src = select(1, ...)
		local dst = {}
		for i = 1, #src do
			local v = src[i]
			local k = i
			if ]] .. aCondition .. [[ then
				table.insert( dst, v)
			end
		end
		return dst
	]]

	return load(code)(aTable)
end

function table.map2arr(aTable)
	local t = {}
	for _,v in pairs(aTable) do
		t[#t + 1] = v
	end
	return t
end

function table.arr2map(aTable, aKey)
	local t = {}
	for i = 1, #aTable do
		local key = aTable[i][aKey]
		if key then
			t[key] = aTable[i]
		end
	end
	return t
end

function table.sortByField(aTable, aOrderBy, aDescend)
	local s = aDescend and ' > ' or ' < '

	local code = [[
	local t = select(1, ...)
	table.sort(t, function(a,b)
	return a.]] .. aOrderBy .. s .. [[b.]] .. aOrderBy .. [[
	end)
	return t
	]]

	return load(code)(aTable)
end

function table.sortByFunc(aTable, aFunc)
	table.sort(aTable, function (a,b)
		return aFunc(a) < aFunc(b)
	end)
end

function table.walk(aTable, aFunc)
	for k,v in pairs(aTable) do
		if type(v) == 'table' then
			table.walk(v, aFunc)
		else
			aFunc(v)
		end
	end
end

do
	local removeLabel = {}

	function table.pendingRemoveMark(aTable, i)
		aTable[i] = removeLabel
	end

	function table.pendingRemoveDone(aTable)
		local i,o,c = 1,1,0

		while aTable[i] ~= nil do
			if aTable[i] == removeLabel then
				c = c + 1
			else
				aTable[o] = aTable[i]
				o = o + 1
			end
			i = i + 1
		end

		for i = #aTable, #aTable-c+1, -1 do
			aTable[i] = nil
		end
	end

	function table.pendingRemoveDoneA(aTable)
		local toRemove = {}
		for k,v in pairs(aTable) do
			if v == removeLabel then
				toRemove[#toRemove + 1] = k
			end
		end
		for i = 1, #toRemove do
			aTable[toRemove[i]] = nil
		end
	end
end

function table.repairNumberIndex(aTable)
	local add = {}
	local rem = {}
	for key, val in pairs(aTable) do
		if type(val) == 'table' then
			table.repairNumberIndex(val)
		end

		local index = tonumber(key)
		if index and type(key) == 'string' then
			rem[key] = true
			add[index] = val
		end
	end

	for key in pairs(rem) do
		aTable[key] = nil
	end
	for key, val in pairs(add) do
		aTable[key] = val
	end
end

function table.array2matrix(t, cols)
	local m = {}
	local rows = math.ceil(#t / cols)
	for row = 1, rows do
		m[row] = {}
		for col = 1, cols do
			m[row][col] = t[col+(row-1)*cols]
		end
	end
	return m
end

function table.shuffle(t)
	for i = #t, 2, -1 do
		local j = math.random(i)
		t[i], t[j] = t[j], t[i]
	end
	return t
end

do
	local defaultMt = {
		__index = function(t,k)
			return t[2][k] or t[1][k]
		end;
		__newindex = function(t,k,v)
			t[2][k] = v
		end;
	}

	function table.default(aDefault, aCustom)
		return setmetatable({ aDefault, aCustom }, defaultMt)
	end
end

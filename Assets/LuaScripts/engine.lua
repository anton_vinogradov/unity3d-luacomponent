UnityEngine    = luanet.UnityEngine
GameObject     = UnityEngine.GameObject
Vector2        = UnityEngine.Vector2
Vector3        = UnityEngine.Vector3
Vector4        = UnityEngine.Vector4
Bounds         = UnityEngine.Bounds
Color          = UnityEngine.Color
AnimationCurve = UnityEngine.AnimationCurve
Rect           = UnityEngine.Rect
LuaController  = luanet.LuaController
LuaComponent   = luanet.LuaComponent
UComponent     = UnityEngine.Component
Transform      = UnityEngine.Transform
Shader         = UnityEngine.Shader
RenderTexture  = UnityEngine.RenderTexture
Log            = UnityEngine.Debug.Log
Time           = UnityEngine.Time

EditorGUILayout = luanet.UnityEditor.EditorGUILayout
GUIHelper       = luanet.LuaGUIHelper
GUILayout       = UnityEngine.GUILayout


function print(...)
	local args = { ... }
	for i = 1, #args do
		args[i] = tostring(args[i])
	end
	local out = table.concat(args, ' ')
	Log(out)
end

function printError(...)
	local args = { ... }
	for i = 1, #args do
		args[i] = tostring(args[i])
	end
	local out = table.concat(args, ' ')
	UnityEngine.Debug.LogError(out)
end

function safeCall(aFunc, ...)
	local ok, r = pcall(aFunc, ...)
	if not ok then
		printError(r)
	end
	return r
end

function checkString(str, name)
	local func, message = assert(loadstring(str, name))
	if not func then return false, message end
	return true
end

function doString(str, name)
	local func, message = assert(loadstring(str, name))
	if not func then return false, message end
	return pcall(func)
end

function typeOf(typeName)
	return LuaComponent.GetType(typeName)
end

function typeOfUnity(typeName)
	return UnityEngine.Types.GetType("UnityEngine."..typeName, "UnityEngine")
end

function RequireLua(name)
	return doString(luaScripts[name], name)
end


function IsNull(object)
	return object:Equals(nil)
end





-- ClassTable = {}

-- function RegisterClass( src, name)
-- 	if _G[name] then return end

-- 	-- print(name, src)
-- 	local func = assert(loadstring(src, name))
-- 	local t = safeCall(func)

-- 	behaviorClass(name)(unpack(t))
-- end

-- function newO(metatable, ...)
-- 	local object = {}

-- 	setmetatable(object, metatable)

-- 	local constructor = rawget(metatable, '__init')
-- 	if (constructor ~= nil) then
-- 		constructor(object, ...)
-- 	end

-- 	return object
-- end

-- function CollectMethods(aClass, aMethodName, aList)
-- 	aList = aList or {}
-- 	for i = 1, #aClass.parents do
-- 		CollectMethods(aClass.parents[i], aMethodName, aList)
-- 	end

-- 	if aClass[aMethodName] then
-- 		aList[#aList + 1] = { class = aClass, method = aClass[aMethodName] }
-- 	end
-- 	return aList
-- end

-- function BuildClass(aClass, aMethodName, aDestName)
-- 	local code = {}
-- 	local list = CollectMethods(aClass, aMethodName)
-- 	local map = {}

-- 	for i = 1, #list do
-- 		local item = list[i]
-- 		if not map[item.method] then
-- 			code[#code + 1] = '_G["' .. item.class.__class .. '"].' .. aMethodName .. '(...);\n'
-- 			map[item.method] = true
-- 		end
-- 	end

-- 	aClass[aDestName] = assert(loadstring(table.concat(code), aDestName))
-- end

-- function behaviorClass(name)
-- 	local metatable = {}
-- 	local env = getfenv(2)

-- 	metatable.__class = name
-- 	metatable.__index = metatable

-- 	setmetatable(metatable, { __call = newO })

-- 	rawset(env, name, metatable)

-- 	return function (...)
-- 		local args = { ... }
-- 		for i = 1, #args do
-- 			local parent = args[i]

-- 			for k in pairs(parent) do
-- 				if k ~= '__index' and k ~= '__class' and k ~= "inspectable" and k ~= "inheritable"  then
-- 					metatable[k] = parent[k]
-- 				end
-- 			end
-- 			if parent.inspectable then
-- 				metatable.inspectable = table_add(metatable.inspectable or {}, parent.inspectable)
-- 			end
-- 			if parent.inheritable then
-- 				metatable.inheritable = table_addi(metatable.inheritable or {}, parent.inheritable)
-- 			end
-- 		end

-- 		args[#args] = nil
-- 		metatable.parents = args

-- 		for _,method_name in pairs(metatable.inheritable) do
-- 			BuildClass(metatable, method_name, '__' .. method_name)
-- 		end
-- 	end
-- end



-- function newLuaObject(classname)
-- 	if classname and _G[classname] then
-- 		return LuaComponent.Create(classname)
-- 	end
-- 	return GameObject("LuaObject"):AddComponent("LuaObject").luaObject
-- end

-- function LoadInstance(path)
-- 	return 	GameObject.Instantiate(UnityEngine.Resources.Load(path)):GetComponent("LuaComponent").luaObject
-- end

-- function LuaInstantiate(object)
-- 	return 	LuaComponent.Copy(object)
-- end

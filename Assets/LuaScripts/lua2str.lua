function isIdentifier(str)
  return str:match'^[%a_][%w_]*$'
end

function quoteStr(str)
  return ('%q'):format(str)
end

function dostring(str)
  local func = loadstring(str)
  return pcall(func)
end

function strToLua(str)
  local init = 'local null = 0/0;';
  local ok, data = dostring(init .. 'return ' .. str)
  if ok then
    return data
  end
  return str
end

local rules = {}

function luaToStr(val)
  return rules[type(val)](val)
end

rules = {
  ['nil'     ] = function(x) return 'nil' end,
  ['boolean' ] = function(x) return x and 'true' or 'false' end,
  ['string'  ] = function(x) return quoteStr(x) end,
  ['cdata'   ] = function(x) return quoteStr(tostring(x)) end,
  ['userdata'] = function(x) return quoteStr(tostring(x)) end,

  ['number'  ] = function(x)
    return rawequal(x,x) and tostring(x) or 'null'
  end,

  ['function'] = function(x)
    if debug.getinfo(x) == 'Lua' then
      return 'loadstring[[' .. string.dump(x) .. ']]'
    else
      return quoteStr(tostring(x))
    end
  end,

  ['table'] = function(x)
    local t = {}

    local i = 1
    while x[i] ~= nil do
      t[#t+1] = luaToStr(x[i])
      i = i + 1
    end

    for k,v in pairs(x) do
      if type(k) ~= 'number' or k >= i or k < 1 then
        if type(k) == 'string' and isIdentifier(k) then
          t[#t+1] = k .. '=' .. luaToStr(v)
        else
          t[#t+1] = '[' .. luaToStr(k) .. ']=' .. luaToStr(v)
        end
      end
    end

    return '{' .. table.concat(t, ',') .. '}'
  end
}

function printLua(...)
  local args = { ... }
  for i = 1, #args do
    print(luaToStr(args[i]))
  end
end

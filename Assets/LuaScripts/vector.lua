class 'Vec3'
local Vector3 = UnityEngine.Vector3

function Vec3:__init(x,y,z)
	self.x = x or 0
	self.y = y or 0
	self.z = z or 0

end
Vec3.one = Vec3(1,1,1)
Vec3.zero = Vec3(0,0,0)


function Vec3:isVector()
	return getmetatable(self) == Vec3
end

function Vec3:clone()
	return Vec3(self.x, self.y, self.z)
end

function Vec3:unpack()
	return self.x, self.y, self.z
end


function Vec3:__unm()
	return Vec3(-self.x, -self.y, -self.z)
end

function Vec3:__add(b)
	return Vec3(self.x + b.x, self.y + b.y, self.z + b.z)
end

function Vec3:__sub(b)
	return Vec3(self.x - b.x, self.y - b.y, self.z - b.z)
end

function Vec3:__mul(b)
	if type(b) == "number" then
		return Vec3(self.x * b, self.y * b, self.z * b)
	else
		return Vec3(self.x * b.x, self.y * b.y, self.z * b.z)
	end
end

function Vec3:__div(b)
	if type(b) == "number" then
		return Vec3(self.x / b, self.y / b, self.z / b)
	else
		return Vec3(self.x / b.x, self.y / b.y, self.z / b.z)
	end
end

function Vec3.__eq(b)
	return self.x == b.x and self.y == b.y and self.z == b.z
end

function Vec3:__tostring()
	return "Vec3(" .. self.x .. "," .. self.y.. "," .. self.z..")"
end

function Vec3:__concat(b)
	return tostring(self) .. tostring(b)
end


function Vec3:dot(b)
	return self.x * b.x + self.y * b.y + self.z * b.z
end

function Vec3:cross(b)
	return self.y * b.x - self.x * b.y
end

function Vec3:length()
	return math.sqrt(self.x ^ 2 + self.y ^ 2 + self.z ^ 2)
end

function Vec3:lengthSquared()
	return self.x ^ 2 + self.y ^ 2 + self.z ^ 2
end

function Vec3:angle()
	return math.atan2(self.y, self.x)
end

function Vec3:normalize()
	local len = self:length()
	if len == 0 then return Vec3.zero end
	self.x = self.x / len
	self.y = self.y / len
	self.z = self.z / len
	return self
end

-----------------------------------------------------------
--	Additional
-----------------------------------------------------------
function Vec3:distance(b)
	return (self - b):length()
end

function Vec3:distanceSquared(b)
	return (self - b):lengthSquared()
end

function Vec3:average(b)
	return (self + b) * 0.5
end

function Vec3:minmax(min, max)
	local x = math.min(math.max(self.x, min.x), max.x)
	local y = math.min(math.max(self.y, min.y), max.y)
	local z = math.min(math.max(self.z, min.z), max.z)
	return Vec3(x, y, z)
end

function Vec3:min()
	
	return math.min(self.x,self.y,self.x )
end

function Vec3:max()
	
	return math.max(self.x,self.y,self.x )
end


function Vec3:projectionOn(b)
	return (self * b) * b / b:lengthSquared()
end

function Vec3:toVector3()
	return Vector3 (self.x, self.y, self.z)
end

function angleDifference(aAngleA, aAngleB)
	local	cycledA = cycled(aAngleA, 360)
	local	cycledB = cycled(aAngleB, 360)
	return	cycledA - cycledB
end

function cycled(aValue, aMax)
	return (aValue - 1) % aMax + 1
end

function radToDeg(aAngle)
	return aAngle * 57.295779513082
end

function degToRad(aAngle)
	return aAngle * 0.017453292519
end

-- function Vector3.Vec3(...)
	-- local a = select(1, ...)
	-- if a then
		-- if type(a) == 'table' or type(a) == 'userdata' then
			-- return Vec3(a.x or a[1] or 0, a.y or a[2] or 0 , a.z or a[3] or 0)
		-- else
			-- return Vec3(...)
		-- end
	-- else
		-- return Vec3(0, 0, 0)
	-- end
-- end

local function new(metatable, ...)
	local object = {}

	setmetatable(object, metatable)

	local constructor = rawget(metatable, '__init')
	if (constructor ~= nil) then
		constructor(object, ...)
	end

	return object
end

local function prepare_class(name)
	local metatable = {}

	metatable.__class	= name
	metatable.__index	= metatable

	setmetatable(metatable, { __call = new })

	return metatable
end

function inherit(metatable, parents)
	for i = 1, #parents do
		local parent = parents[i]
		for k in pairs(parent) do
			if k ~= '__index' and k ~= '__class' then
				metatable[k] = parent[k]
			end
		end
	end
end

function class(name)
	local metatable = prepare_class(name)
	local env = getfenv(2)

	rawset(env, name, metatable)

	return function (...)
		inherit(metatable, { ... })
	end
end

function local_class(name)
	return prepare_class(name)
end


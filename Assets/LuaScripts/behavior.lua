-----------------------------------------------------------
--	Classes
-----------------------------------------------------------
BehaviorsTable = {}

function RegisterClass(aName)
	_G[aName] = {}
	table.insert(BehaviorsTable, aName)
end


local Cache = {}

function Hash(ClassList)
	return table.concat(ClassList,':')
end

function GetMethod(aClassName, aMethodName)
	return _G[aClassName][aMethodName]
end

function PrepareMethod(aName, aMethods)
	if #aMethods == 1 then
		return GetMethod(aMethods[1], aName)
	else
		local code = {}
		for _,class in pairs(aMethods) do
			code[#code + 1] = '_G["' .. class .. '"].' .. aName .. '(...);\n'
		end
		return loadstring(table.concat(code), aDestName)
	end
end

function PrepareIndexTable(ClassList)
	local index   = {}
	local methods = {}

	for _,class_name in pairs(ClassList) do
		local class = _G[class_name]
		for name,v in pairs(class) do
			if type(v) == 'function' then
				if methods[name] then
					table.insert(methods[name], class_name)
				else
					methods[name] = { class_name }
				end
			end
		end
	end

	index.__index = index

	for method,list in pairs(methods) do
		index[method] = PrepareMethod(method, list)
	end

	return index
end

function GetIndexTable(ClassList)
	local hash = Hash(ClassList)

	local index = Cache[hash]
	if not index then
		index = PrepareIndexTable(ClassList)
		Cache[hash] = index
	end

	return index
end

function CreateObject(ClassList)
	return setmetatable({}, GetIndexTable(ClassList))
end

function LuaObject(srclist, base)
	local list = {}
	for i = 0, srclist.Count -1 do
		list[i+1] = srclist[i]
	end
	-- srclist = {'BaseClass', "UIText", "UISprite" }
	-- if list[1] ~= 'BaseClass' then
	-- 	table.insert(srclist, 1, 'BaseClass')
	-- end

	local obj = CreateObject(list)

	obj.base = base
	base.mluaObject = obj
	obj.inspectable = {}

	obj:Init()

end


function CheckDependencies(classname, array)
	local dep = _G[classname].dependence
	if not dep then return end
	for i = 1, #dep do
		local contains = false
		for j = 0, array.Count -1 do
			if dep[i] == array[i] then
				contains = true
			end
		end
		if not contains then
			local ind = array:IndexOf(classname)
			array:Insert(ind, dep[i])
			CheckDependencies(dep[i], array)
		end
	end
	

end

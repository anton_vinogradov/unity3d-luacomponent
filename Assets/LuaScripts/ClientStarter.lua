function StartAll ()
	print("Start client", MainLuaController)
	networkView = MainLuaController:GetComponent("NetworkView")
	RPCcommands = MainLuaController:GetComponent("RPCcommands")
end
RPCcommands = nil
networkView = nil
teststring = ""
updateTable = {}
mousedTable = {}
function UpdateAll(aDelta)

	for k,v in pairs(updateTable) do
		if v and v.base.isActiveAndEnabled then
			v:__Update(aDelta)
		end
	end
end

function MouseDown(id, mousePos)
	for k,v in pairs(mousedTable) do
		if v and v.base.isActiveAndEnabled then
			v:__MouseDown(id, mousePos)
		end
	end
end

function MouseUp(id, mousePos)
	for k,v in pairs(mousedTable) do
		if v and v.base.isActiveAndEnabled then
			v:__MouseUp(id, mousePos)
		end
	end
end

function MouseMove(id, deltaPos)
	for k,v in pairs(mousedTable) do
		if v and v.base.isActiveAndEnabled then
			v:__MouseMove(id, deltaPos)
		end
	end
end

function LuaOnGui()
	if GUILayout.Button("Request timer", {}) then
		local server = RPCcommands.serverPl
		local lua = "LuaOnGui = function ()	GUILayout.Label('new Timer + '..testTimer, {})	end "
		lua = lua .. "MainLuaController:GetFunctions()"
		local callbackid = ""
		-- teststring = "wait for answer"
		-- callbacks[callbackid] = function ( val )
		-- 	teststring = "result "..val
		-- end
		--print(player:ToString())

		RPCcommands:netRPC("DoLuaCode", server, {RPCcommands.selfPl, lua, callbackid }  )
	end
	GUILayout.Label(teststring, {})
end

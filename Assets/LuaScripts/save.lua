function saveTable(aTable, aPath)
	local f = assert(io.open(aPath, "wb"))
	for key, data in pairs(aTable) do
		f:write(key .. ' = ' .. luaToStr(data) .. '\n')
	end
	f:close()
end

function loadTable(aPath)
	local table = {}
	if fileExists(aPath) then
		local src = fileReadAll(aPath)
		local func = assert(loadstring(src))
		setfenv(func, table)
		pcall(func)
	end
	return table
end

function setTableValueP(root, path, value)
	for i = 1, #path - 1 do
		local section = root[path[i]]
		if not (section and type(section) == 'table') then
			section = {}
			root[path[i]] = section
		end
		root = section
	end

	root[path[#path]] = value
end

function getTableValueP(root, path, default)
	for i = 1, #path - 1 do
		local section = root[path[i]]
		if not (section and type(section) == 'table') then
			return default
		end
		root = section
	end

	if root[path[#path]] ~= nil then
		return root[path[#path]]
	else
		return default
	end
end

function setTableValue(root, key, value)
	setTableValueP(root, key:split('.'), value)
end

function getTableValue(root, key, default)
	return getTableValueP(root, key:split('.'), default)
end

function exportSaveTable(aRoot, aFunc, aKey)
	aKey = aKey or ''

	for k,v in pairs(aRoot) do
		local key = #aKey > 0 and (aKey..'.'..k) or k

		if type(v) == 'table' then
			exportSaveTable(v, aFunc, key)
		else
		   aFunc(key, v)
		end
	end
end

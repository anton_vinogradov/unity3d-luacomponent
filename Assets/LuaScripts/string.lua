function string.split(aStr, aDelimiter)
	local result = {};

	local start = 1
	local from, to = string.find(aStr, aDelimiter, 1, true)
	while from do
		result[#result + 1] = string.sub(aStr, start, from - 1)
		start = to + 1
		from, to = string.find(aStr, aDelimiter, start, true)
	end
	result[#result + 1] = string.sub(aStr, start)

	return result
end

function string.interpolate(aStr, aTable)
	local result = aStr:gsub('(%%{.-})', function(w)
		local key = w:sub(3, -2)
		return aTable[tonumber(key) or key] or w
	end)

	return result
end

function string.totable(aStr)
	local result = {}
	for char in aStr:gmatch'.' do
		result[#result+1] = char
	end
	return result
end

local randomChars = "1234567890QWERTYUIOPASDFGHJKLZXCVBNMMqwertyuiopasdfghjklzxcvbnm"
function string.random(aLength)
	local ret = ""
	local len = #randomChars
	for i = 1, aLength do
		ret = ret .. string.char(string.byte(randomChars, math.random(len)))
	end

	return ret
end

function fuzzy_match(aSet, aQuery, aHighlight)
	local tokens  = aQuery:lower():totable()
	local matches = {}

	aHighlight = aHighlight or function(s)
		return s
	end

	for i = 1, #aSet do
		local tokenIndex  = 1
		local stringIndex = 1
		local matchWithHighlights = ''
		local matchedPositions = {}
		local str = aSet[i]:lower()

		while stringIndex <= #str do
			local letter = str:sub(stringIndex,stringIndex)
			if letter == tokens[tokenIndex] then
				matchWithHighlights = matchWithHighlights .. aHighlight(letter)
				matchedPositions[#matchedPositions+1] = stringIndex
				tokenIndex = tokenIndex + 1
				if tokenIndex > #tokens then
					matches[#matches+1] = {
						index = i;
						match = str;
						highlighted = matchWithHighlights .. str:sub(stringIndex + 1);
						positions = matchedPositions;
					}
					break
				end
			else
				matchWithHighlights = matchWithHighlights .. letter
			end
			stringIndex = stringIndex + 1
		end
	end
	return matches
end

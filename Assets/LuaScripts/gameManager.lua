class 'GameManager'

function GameManager:__init()
	self.mSave = {}

	self.objects = {}
	-- self.tasks = {}
	self.savePath = UnityEngine.Application.persistentDataPath ..'/worldsave.txt';

	-- local human = base()
	-- human:AbilityAdd("DieOnce")
	local firstE = Collection["EtherLord"]()
	firstE.name = "IM"
	local someobj = Collection["EnergyStone"]()
	firstE:AddObject(someobj)

	-- local act = action()
	-- act:Create()
	-- act:Combine("Rest","Rest","Rest")
	-- act:Start (firstE)

	local Squad = Collection["Squad"]()
	Squad:AddObject(firstE)
	-- Squad:RemoveObject(firstE)

	self.objects.firstE = firstE
	self.objects.someobj = someobj
	self.objects.Squad = Squad
end


function GameManager:Save()

	-- for k,v in pairs(self.players) do
	-- 	self.mSave.players[k] = v.Save()
	-- end

	-- for k,v in pairs(self.tasks) do
	-- 	self.mSave.tasks[k] = v.Save()
	-- end

	saveTable(self.mSave, self.savePath)
end

function GameManager:Load()
	self.mSave = loadTable(self.savePath)
	-- self.mSave.players = self.mSave.players or {}
	

	-- local players = self.mSave.players
	-- for k,v in pairs(players) do
	-- 	self.players[k] = GameWorker()
	-- 	self.players[k]:Load(v)
	-- end
	-- self.mSave.tasks = self.mSave.tasks or {}
	-- local tasks = self.mSave.tasks
	-- for k,v in pairs(tasks) do
	-- 	self.tasks[k] = WorkTask()
	-- 	self.tasks[k]:Load(v)
	-- end
end

function GameManager:AddPlayer(login, password)
	-- local newPlayer =  GameWorker()
	-- newPlayer:setLogin(login, password)
	-- self.players[login] = newPlayer
	-- self:Save()
end


function GameManager:CheckPlayer(login)
	-- return self.players[login] ~= nil
end

function GameManager:Update(aDelta)
	for k,v in pairs(self.objects) do
		v:Update(aDelta)
	end
end

-----------------------------------------------------------
--	Time Common
-----------------------------------------------------------
function getGlobalTime()
	-- if kt.System.isInetAvailable() then
		-- return kt.HttpClient.getTime()
	-- end

	return os.time()
end

function convertTime(aSeconds)
	return os.date("*t", aSeconds)
end

function convertLocalTime(aSeconds)
	local min  = 60
	local hour = min * 60
	local day  = hour * 24

	local t = {
		day  = 0;
		hour = 0;
		min  = 0;
		sec  = 0;
	}

	if aSeconds > day then
		t.day = math.floor(aSeconds / day)
		aSeconds = aSeconds % (day)
	end
	if aSeconds > hour then
		t.hour = math.floor(aSeconds / hour)
		aSeconds = aSeconds % (hour)
	end
	if aSeconds > min then
		t.min = math.floor(aSeconds / min)
		aSeconds = aSeconds % (min)
	end
	t.sec = math.floor(aSeconds)

	return t
end

function printTime(aSeconds, aIsLocal)
	if aSeconds < 0 then
		return '0'
	end

	local s = ''
	local t = aIsLocal and convertLocalTime(aSeconds) or convertTime(aSeconds)
	if aSeconds > 3600*24 then
		s = t.day..getTag("time.day")..' '..t.hour..getTag("time.hour")
	elseif aSeconds > 3600 then
		s = t.hour..getTag("time.hour")..' '..t.min ..getTag("time.min")
	else
		s = t.min..getTag("time.min")..' '..t.sec ..getTag("time.sec")
	end
	return s
end

function isPassedOneDay(s1, s2)
	local d1 = os.date("*t", s1 + 3600*24)
	local d2 = os.date("*t", s2)

	return d1.yday == d2.yday
end

local timeTable = {
	s = 1;
	m = 60;
	h = 3600;
	d = 3600 * 24;
	w = 3600 * 24 * 7;
}

function stringToSeconds(aString)
	local seconds = 0
	string.gsub(aString, '([0-9%.]+)%s*(%a+)', function(a,b)
		seconds = seconds + tonumber(a) * timeTable[b]
	end)
	return seconds
end

-----------------------------------------------------------
--	CPUTimer
-----------------------------------------------------------
class 'CPUTimer'

function CPUTimer:__init()
	self.mLast	= os.clock()
	self.mPrev	= self.mLast
end

function CPUTimer:update()
	self.mPrev = self.mLast
	self.mLast = os.clock()
	return self.mLast - self.mPrev
end

function CPUTimer:getLast()
	return self.mLast
end

-----------------------------------------------------------
--	LuaInspector
-----------------------------------------------------------

EditorActionPlayTimer = 0
inspectedTypes = {}
initTypes = {}

function returnInspectedType(key,s)
	return initTypes[key](s)
end

initTypes['ActionPlay'] = function()
	return false
end

inspectedTypes['ActionPlay']= function(key,value, object)

	GUILayout.Label("Dur".. object:MainAction().mDuration , {})

	if value and object.timer >= 0 then
		local delta = Time.realtimeSinceStartup - object.editorTime
		BaseClass.AM:update(delta)
		object.editorTime = Time.realtimeSinceStartup
		object.timer = object.timer - delta
		if object.timer < 0 then
			value = false
			object:EditorStop()
		end
	end

	if  not value  then
		if GUILayout.Button("Play", {}) then
			value = true
			object:setSerialized(key, value)
			object:EditorRun()
			object.timer = object:MainAction().mDuration
			object.editorTime = Time.realtimeSinceStartup

		end
	else
		if GUILayout.Button("Stop", {}) then
			value = false
			object:setSerialized(key, value)
			object:EditorStop()


		end
	end
	return value
end

-- inspectedTypes['textTag'] = function(key,value)
	-- if value == nil then
		-- value = ""
	-- end
	-- GUILayout.BeginHorizontal({})
	-- GUILayout.Label(value , {})
	-- if GUILayout.Button("Select", {}) then
		 -- luanet.GameTools.UIlabelTag ()
	-- end
	-- GUILayout.EndHorizontal()
	-- return value
-- end

initTypes['label'] = function(s)
	if s and s.default then
		return s.default
	end
	return ""
end

inspectedTypes['label'] = function(key,value)
	GUILayout.Label(value , {})
	return value
end

initTypes['text'] = function(s)
	if s and s.default then
		return s.default
	end
	return ""
end

inspectedTypes['text'] = function(key,value)
	return 	GUILayout.TextArea( value , {} )
end

initTypes['int'] = function(s)
	if s and s.default then
		return s.default
	end
	return 0
end

inspectedTypes['int'] = function(key, value, obj, s)
	value = EditorGUILayout.IntField(key, value ,{})
	if s then
		if s.min then
			value = math.max(value, s.min)
		end
		if s.max then
			value = math.min(value, s.max)
		end
	end
	return value
end

initTypes['LuaComponent'] = function()
	return nil
end

inspectedTypes['LuaComponent'] = function(key,value)
	local outvalue = EditorGUILayout.ObjectField(key, value, typeOf('LuaComponent'), true,  {})

	if outvalue ~= nil and GUILayout.Button("Clear", {}) then
		outvalue = nil
	end
	return outvalue
end

initTypes['UIWidget'] = function()
	return nil
end

inspectedTypes['UIWidget'] = function(key,value)
	local outvalue = EditorGUILayout.ObjectField(key, value, typeOf('UIWidget'), true,  {})

	if outvalue ~= nil and GUILayout.Button("Clear", {}) then
		outvalue = nil
	end
	return outvalue
end

initTypes['UIWidgetTable'] = function()
	return LuaComponent.GetListOfT( typeOf('UIWidget'))
end

inspectedTypes['UIWidgetTable'] = function(key,value)

	GUILayout.BeginVertical({})
	GUILayout.BeginHorizontal({})
	GUILayout.Label(key ,{})
	GUILayout.FlexibleSpace()
	local capacity =  GUIHelper.IntField("Array size", value.Count)
	while capacity > value.Count do
		value:Add(nil)
	end
	while capacity < value.Count do
		value:RemoveAt(value.Count-1)
	end
	if GUILayout.Button("ClearAll", {}) then
		for i = 0, value.Count - 1  do
			value[i] = nil
		end
	end
	GUILayout.EndHorizontal()

	for i = 0, value.Count - 1  do
		GUILayout.BeginHorizontal({})
		value[i] = EditorGUILayout.ObjectField( value[i]  , typeOf('UIWidget'), true, {})
		if value[i] ~= nil and GUILayout.Button("Clear", {}) then
			value[i] = nil
		end
		GUILayout.EndHorizontal()
	end
	GUILayout.EndVertical()
	GUILayout.Space(5)
	return value

end

initTypes['GameObject'] = function()
	return nil
end

inspectedTypes['GameObject'] = function(key,value)
	local outvalue = EditorGUILayout.ObjectField(key, value , typeOfUnity('GameObject'), true, {})

	if outvalue ~= nil and GUILayout.Button("Clear", {}) then
		outvalue = nil
	end
	return outvalue
end

initTypes['GameObjectTable'] = function()
	return LuaComponent.GetListOfT( typeOfUnity('GameObject'))
end

inspectedTypes['GameObjectTable'] = function(key,value)

	GUILayout.BeginVertical({})
	GUILayout.BeginHorizontal({})
	GUILayout.Label(key ,{})
	GUILayout.FlexibleSpace()
	local capacity =  GUIHelper.IntField("Array size", value.Count)
	while capacity > value.Count do
		value:Add(nil)
	end
	while capacity < value.Count do
		value:RemoveAt(value.Count-1)
	end
	if GUILayout.Button("ClearAll", {}) then
		for i = 0, value.Count - 1  do
			value[i] = nil
		end
	end
	GUILayout.EndHorizontal()

	for i = 0, value.Count - 1  do
		GUILayout.BeginHorizontal({})
		value[i] = EditorGUILayout.ObjectField( value[i]  , typeOfUnity('GameObject'), true, {})
		if value[i] ~= nil and GUILayout.Button("Clear", {}) then
			value[i] = nil
		end
		GUILayout.EndHorizontal()
	end
	GUILayout.EndVertical()
	GUILayout.Space(5)
	return value

end

initTypes['GameObjectsMap'] = function()
	return LuaComponent.GetListOfT( typeOfUnity('GameObject'))
end

inspectedTypes['GameObjectsMap'] = function(key,value)

	GUILayout.BeginVertical({})
	GUILayout.BeginHorizontal({})
	GUILayout.Label(key ,{})
	GUILayout.FlexibleSpace()
	local capacity =  GUIHelper.IntField("Array size", value.Count)
	while capacity > value.Count do
		value:Add(nil)
	end
	while capacity < value.Count do
		value:RemoveAt(value.Count-1)
	end
	if GUILayout.Button("ClearAll", {}) then
		for i = 0, value.Count - 1  do
			value[i] = nil
		end
	end
	GUILayout.EndHorizontal()

	for i = 0, value.Count - 1  do
		GUILayout.BeginHorizontal({})
		value[i] = EditorGUILayout.ObjectField( value[i]  , typeOfUnity('GameObject'), true, {})
		if value[i] ~= nil and GUILayout.Button("Clear", {}) then
			value[i] = nil
		end
		GUILayout.EndHorizontal()
	end
	GUILayout.EndVertical()
	GUILayout.Space(5)
	return value

end

initTypes['Vector2'] = function()
	return Vector2(0,0)
end

inspectedTypes['Vector2'] = function(key,value)
	return EditorGUILayout.Vector2Field(key, value , {})
end

initTypes['Vector3'] = function()
	return Vector3(0,0,0)
end

inspectedTypes['Vector3'] = function(key,value)
	return EditorGUILayout.Vector3Field(key, value , {})
end

initTypes['Vector4'] = function()
	return Vector4(0,0,0,0)
end

inspectedTypes['Vector4'] = function(key,value)
	return EditorGUILayout.Vector4Field(key, value , {})
end

initTypes['float'] = function(s)
	if s and s.default then
		return s.default
	end
	return 0
end

inspectedTypes['float'] = function(key,value)
	value = EditorGUILayout.FloatField(key, value , {})
	if s then
		if s.min then
			value = math.max(value, s.min)
		end
		if s.max then
			value = math.min(value, s.max)
		end
	end
	return value
end

initTypes['Bounds'] = function()
	return Bounds()
end

inspectedTypes['Bounds'] = function(key,value)
	return EditorGUILayout.BoundsField(key, value , {})
end

initTypes['Color'] = function()
	return UnityEngine.Color(1,1,1)
end

inspectedTypes['Color'] = function(key,value)
	return EditorGUILayout.ColorField(key, value, {} )
end

initTypes['Color'] = function()
	return AnimationCurve()
end


inspectedTypes['AnimationCurve'] = function(key,value)
	return EditorGUILayout.CurveField(key, value , {})
end

initTypes['Rect'] = function()
	return Rect()
end

inspectedTypes['Rect'] = function(key,value)
	return EditorGUILayout.RectField(key, value , {})
end

initTypes['string'] = function(s)
	if s and s.default then
		return s.default
	end
	return ""
end

inspectedTypes['string'] = function(key,value)
	return EditorGUILayout.TextField(key, value , {})
end

initTypes['bool'] = function(s)
	if s and s.default then
		return s.default
	end
	return false
end

inspectedTypes['bool'] = function(key,value)
	return EditorGUILayout.Toggle(key, value , {})
end

initTypes['string_enum'] = function(s)
	if s and s.default then
		return s.default
	end
	return ''
end

inspectedTypes['string_enum'] = function(key,value,obj,s)
	if s and s[2] then
		return GUIHelper.StringPopup(key,value,s[2])
	end
	return ''
end

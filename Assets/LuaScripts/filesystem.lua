function fileExists(aPath)
	local file = io.open(aPath, 'r')
	if file then
		file:close()
		return true
	end
	return false
end

function fileReadAll(aPath)
    local file = io.open(aPath, 'rb')
    local data = file:read('*all')
    file:close()
    return data
end

-----------------------------------------------------------
--	Common
-----------------------------------------------------------
function minmax(aValue, aMin, aMax)
	return math.min(math.max(aValue, aMin), aMax)
end

function average(aTable)
	local acc = aTable[1]
	for i = 2, #aTable do
		acc = acc + aTable[i]
	end
	return acc * (1/#aTable)
end

function sign(aValue)
  return aValue > 0 and 1 or (aValue < 0 and -1 or 0)
end

function cycled(aValue, aMax)
	return (aValue - 1) % aMax + 1
end

function roundOff(aValue, aInterval, aAlign)
	aInterval	= aInterval or 1
	aAlign		= aAlign or 0.5

	return math.floor(aValue / aInterval + aAlign) * aInterval
end

function approximate(aVariable, aValue, aError)
	if math.abs(aValue - aVariable) < aError then
		return aValue
	end
	return aVariable
end

function convertRange(value, aFromMin, aFromMax, aToMin, aToMax)
	local	unit = (value - aFromMin) / (aFromMax - aFromMin)
	return	unit * (aToMax - aToMin) + aToMin
end


function radToDeg(aAngle)
	return aAngle * 57.295779513082
end

function degToRag(aAngle)
	return aAngle * 0.017453292519
end

function angleDifference(aAngleA, aAngleB)
	local	diff = aAngleA - aAngleB
	return	math.atan2(math.sin(diff), math.cos(diff))
end


-----------------------------------------------------------
--	Tween
-----------------------------------------------------------
function bounceTime(aTime)
	if aTime < 1.0 / 2.75 then
		return 7.5625 * aTime * aTime
	elseif aTime < 2.0 / 2.75 then
		aTime = aTime - 1.5 / 2.75
		return 7.5625 * aTime * aTime + 0.75
	elseif aTime < 2.5 / 2.75 then
		aTime = aTime - 2.25 / 2.75
		return 7.5625 * aTime * aTime + 0.9375
	end

	aTime = aTime - 2.625 / 2.75
	return 7.5625 * aTime * aTime + 0.984375
end


-----------------------------------------------------------
--	Phys
-----------------------------------------------------------
function physNumbers(aNumA, aNumB, aLength, aForce, aBalance, aCondition)
	aForce		= aForce or 1
	aBalance	= aBalance or 0.5
	aCondition	= aCondition or 0

	local diff = aNumA - aNumB
	local need = math.abs(diff) - aLength

	if diff == 0 then
		diff = math.random(-1,1)
	end

	if need * aCondition <= 0 then
		local offset = math.sign(diff) * need * aForce

		aNumA = aNumA - offset * aBalance
		aNumB = aNumB + offset * (1 - aBalance)
	end

	return aNumA, aNumB
end

function physVectors(aVecA, aVecB, aLength, aForce, aBalance, aCondition)
	aForce		= aForce or 1
	aBalance	= aBalance or 0.5
	aCondition	= aCondition or 0

	local diff = aVecA - aVecB
	local need = diff:length() - aLength

	if diff == Vec2(0,0) then
		diff = randomVector(-1,1, -1,1)
	end

	if need * aCondition <= 0 then
		local offset = diff:normalize() * need * aForce

		aVecA = aVecA - offset * aBalance
		aVecB = aVecB + offset * (1 - aBalance)
	end

	return aVecA, aVecB
end


-----------------------------------------------------------
--	Vectors
-----------------------------------------------------------
function randomVector(aFromX, aToX, aFromY, aToY)
	local posX = math.random(aFromX, aToX)
	local posY = math.random(aFromY, aToY)

	return Vec2(posX, posY)
end

function randomVectorByLen(aMaxLength, aMinLength)
	aMinLength = aMinLength or 0

	local angle		= math.random(-math.pi, math.pi)
	local length	= math.random(aMinLength, aMaxLength)

	local posX = math.cos(angle) * length
	local posY = math.sin(angle) * length

	return Vec2(posX, posY)
end

function moveFromTo(aFrom, aTo, aStep)
	local diff = aTo - aFrom

	if diff:length() > aStep then
		return	aFrom + diff:normalize() * aStep
	end

	return aTo
end


function segmentIntersect(p1, p2, p3, p4)
	local s1 = p2 - p1
	local s2 = p4 - p3

	local s = (-s1.y * (p1.x - p3.x) + s1.x * (p1.y - p3.y)) / (-s2.x * s1.y + s1.x * s2.y)
	local t = ( s2.x * (p1.y - p3.y) - s2.y * (p1.x - p3.x)) / (-s2.x * s1.y + s1.x * s2.y)

	if s >= 0 and s <= 1 and t >= 0 and t <= 1 then
		return Vec2(p1.x + (t * s1.x), p1.y + (t * s1.y))
	end
end

function getPointToSegmentDistance(aPoint, aA, aB)
	local distance = vectorsDistance

	local Ls = vectorsDistanceSquared(aA, aB)
	if Ls == 0 then
		return distance(aPoint, aA)
	end

	local t = (aPoint - aA) * (aB - aA) / Ls
	if		t < 0 then
		return distance(aPoint, aA)
	elseif	t > 1 then
		return distance(aPoint, aB)
	end

	local projection = aA + t * (aB - aA)
	return distance(aPoint, projection)
end

function getAngleFromTo(aFrom, aTo)
	local v = aTo - aFrom
	local angle = math.atan2(v.y, v.x)
	if angle < 0 then
		return angle + math.pi * 2
	end
	return angle
end

function getPosByAngle(aAngle, aRadius, aCenter)
	aCenter	= aCenter or Vec(0,0)
	aRadius	= aRadius or 1

	local x = aRadius * math.cos(aAngle)
	local y = aRadius * math.sin(aAngle)

	return Vec2(x,y) + aCenter
end


-----------------------------------------------------------
--	Graph
-----------------------------------------------------------
class 'GraphNode'

function GraphNode:__init(aId, aData)
	self.id		= aId
	self.edges	= {}
	self.nodes	= {}
	self.data 	= aData or {}
end

function GraphNode:getDegree()
	return #self.edges
end

function GraphNode:isAdjacent(aNode)
	for i = 1, #self.nodes do
		if rawequal(self.nodes[i], aNode) then
			return true
		end
	end
	return false
end


class 'GraphEdge'

function GraphEdge:__init(aFrom, aTo, aData)
	self.nodes	= {aFrom, aTo}
	self.direct = false
	self.data	= aData or {}
end

function GraphEdge:makeDirected()
	self.direct = true
end

function GraphEdge:makeUndirected()
	self.direct = false
end

function GraphEdge:swapDirection()
	local nodes	= self.nodes
	self.nodes	= {nodes[2], nodes[1]}
end

function GraphEdge:getAdjacent(aCurrent)
	if rawequal(self.nodes[1], aCurrent) then
		return self.nodes[2]
	end
	return self.nodes[1]
end


class 'Graph'

function Graph:__init()
	self.mRoot = false

	self.mNodes = {}
	self.mEdges = {}
	self.mNodesMap = {}
	self.mEdgesMap = {}
end

function Graph:setRoot(aRoot)
	self.mRoot = aRoot
end

function Graph:addNode(aData)
	local id	= #self.mNodes + 1
	local node	= GraphNode(id, aData)

	self.mNodes[id] = node
	self.mNodesMap[node] = id

	return node
end

function Graph:addEdge(aFrom, aTo, aData)
	local edge = GraphEdge(aFrom, aTo, aData)

	table.insert(self.mEdges,	edge)

	table.insert(aFrom.edges,	edge)
	table.insert(aTo.edges, 	edge)

	table.insert(aFrom.nodes, 	aTo)
	table.insert(aTo.nodes, 	aFrom)

	self.mEdgesMap[edge] = #self.mNodes

	return edge
end

function Graph:getOrder()
	return #self.mNodes
end

function Graph:getSize()
	return #self.mEdges
end

function Graph:getAnyNode()
	return anyIn(self.mNodes)
end

function Graph:walkFrom(aFrom, aFunc, ...)
	local visited = {}
	local currArr = {aFrom}
	aFunc(aFrom, ...)
	repeat
		local nextArr = {}
		for _, node in ipairs(currArr) do
			for _, adjacent in ipairs(node.nodes) do
				if not visited[adjacent] then
					table.insert(nextArr, adjacent)
					aFunc(adjacent, ...)
					visited[adjacent] = true
				end
			end
		end
		currArr = nextArr
	until (#currArr == 0)
end

function Graph:getIsolatedNodes(aFrom)
	local isolated = {}

	self:walkFrom(aFrom, function (node, list)
		list[#list+1] = node
	end, isolated)

	return isolated
end

function Graph:getMatchedNodes(aFrom, aFunc, aArgs)
	local matched = {}

	self:walkFrom(aFrom, function (node, list)
		if aFunc(node, list, aArgs) then
			list[#list+1] = node
		end
	end, matched)

	return matched
end

function Graph:getAnyAccessibleNode(aFrom)
	local isolated = self:getIsolatedNodes(aFrom)
	return anyIn(isolated)
end

function Graph:getPathBetween(aFrom, aTo)
	for i = 1, #self.mNodes do
		self.mNodes[i].from = false
	end

	if rawequal(aFrom, aTo) then
		return {aFrom, aTo}
	end

	local currArr = {aTo}
	repeat
		local nextArr = {}

		for _, node in ipairs(currArr) do
			if rawequal(node, aFrom) then
				local path = {node}
				repeat
					node = node.from
					table.insert(path, node)
				until rawequal(node, aTo)
				return path
			end

			for _, adjacent in ipairs(node.nodes) do
				if not adjacent.from then
					table.insert(nextArr, adjacent)
					adjacent.from = node
				end
			end
		end
		currArr = nextArr
	until (#currArr == 0)

	return false
end

function Graph:assignPositions()
	for i = 1, #self.mNodes do
		self.mNodes[i].pos = randomVector(-512,512, -512,512)
	end

	for i = 1, 100 do
		for i = 1, #self.mNodes - 1 do
			local nodea = self.mNodes[i]

			for j = i + 1, #self.mNodes do
				local nodeb = self.mNodes[j]

				if nodea:isAdjacent(nodeb) then
					nodea.pos, nodeb.pos = physVectors(nodea.pos, nodeb.pos, 196, 0.5, 0.5, -1)
				end
				if nodea:getDegree() > 0 and nodeb:getDegree() > 0 then
					nodea.pos, nodeb.pos = physVectors(nodea.pos, nodeb.pos, 1024, 0.01, 0.5, 1)
					nodea.pos = physVectors(nodea.pos, Vec2(0,0), 512, 0.1, 0.5, -1)
					nodeb.pos = physVectors(nodeb.pos, Vec2(0,0), 512, 0.1, 0.5, -1)
				else
					nodea.pos, nodeb.pos = physVectors(nodea.pos, nodeb.pos, 196, 0.5, 0.5, 1)
				end
			end
		end
	end
end

function Graph:getView(aDrawNodeFunc, aDrawEdgeFunc, aArgs)
	for i = 1, #self.mEdges do
		local edge = self.mEdges[i]
		local nodea, nodeb = edge.nodes[1], edge.nodes[2]
		aDrawEdgeFunc(nodea, nodeb, aArgs)
	end
	for i = 1, #self.mNodes do
		local node = self.mNodes[i]
		aDrawNodeFunc(node, aArgs)
	end
end

-- function Graph:getView(aNodeData, aEdgeData, aFunc)
-- 	local root = kt.RGBANode2D.create()
-- 	for i = 1, #self.mEdges do
-- 		local edge = self.mEdges[i]
-- 		local nodea, nodeb = edge.nodes[1], edge.nodes[2]
-- 		local angle = getAngleFromTo(nodea.pos, nodeb.pos)

-- 		local sprite = kt.Sprite.create(aEdgeData.size, aEdgeData.color)

-- 		local size	= sprite:getSize()
-- 		local hh	= 0

-- 		sprite:setPosition(nodea.pos)
-- 		sprite:setRotation(angle)
-- 		sprite:setPivot(hh,hh)
-- 		sprite:setColor(kt.Color(0,0,0, 255))

-- 		local dist = vectorsDistance(nodea.pos, nodeb.pos)
-- 		local scaleX = dist / size.width
-- 		local scaleY = 4 / size.height
-- 		sprite:setScale(scaleX, scaleY)

-- 		root:addChild(sprite, 1)
-- 	end

-- 	for i = 1, #self.mNodes do
-- 		local node = self.mNodes[i]
-- 		local sprite = kt.Sprite.create(aNodeData.size, aNodeData.color)

-- 		sprite:setPosition(node.pos)
-- 		aFunc(i, node, sprite)
-- 		root:addChild(sprite, 1)
-- 	end

-- 	return root
-- end

DBEditor = {}

UnityEditor = luanet.UnityEditor

local aux = {}
local typesGUI = {}

function LoadDataBase(aPath)
	local db = RequireLua(aPath)

	if db then
		for _,name in pairs(db.info.tables) do
			local key = false

			for _,sheme in pairs(db.sheme[name]) do
				if sheme.key == true then
					key = sheme.name
					break
				end
			end

			if key then
				local table = {}
				for _,record in pairs(db.data[name]) do
					table[record[key]] = record
				end
				db.data[name] = table
			end
		end
	else
		printError(aPath .. ' loading failed: ' .. data)
	end

	return db.data
end


function DBEditor:Save(aPath)
	local db = self.mDB
	local file = assert(io.open(aPath, "wb"))

	file:write('local info = {}\n\n')

	file:write('info.tables = {\n')
	for _,name in pairs(db.info.tables) do
		file:write('\t' .. luaToStr(name) .. ';\n')
	end
	file:write('}\n\n')
	file:write('\n')

	file:write('local types = {}\n\n')
	file:write('\n')

	file:write('local macro = {}\n\n')
	for _,name in pairs(db.info.tables) do
		file:write('macro.' .. name .. ' = ' .. luaToStr(db.macro[name]) .. '\n')
	end
	file:write('\n\n')

	file:write('local sheme = {}\n\n')
	for _,name in pairs(db.info.tables) do
		file:write('sheme.' .. name .. ' = {\n')
		for _,sheme in pairs(db.sheme[name]) do
			file:write('\t' .. luaToStr(sheme) .. ';\n')
		end
		file:write('}\n\n')
	end
	file:write('\n')

	file:write('local data = {}\n\n')
	for _,name in pairs(db.info.tables) do
		file:write('data.' .. name .. ' = {\n')
		local sheme = db.sheme[name]
		for _,rec in pairs(db.data[name]) do
			file:write('\t' .. self:RecordAsString(rec, sheme) .. ';\n')
		end
		file:write('}\n\n')
	end
	file:write('\n')

	file:write("return { info = info; types = types; macro = macro; sheme = sheme; data = data; }")

	file:close()

	print(aPath .. ' saved;')
end

function DBEditor:RecordAsString(aRec, aSheme)
	local t = {}
	for _,col in pairs(aSheme) do
		t[#t + 1] = col.name .. '=' .. luaToStr(aRec[col.name])
	end
	return '{ ' .. table.concat(t, ', ') .. ' }'
end

function DBEditor:Load(aPath)
	local file = io.open(aPath, 'rb')
	if file then
		local ok, data = dostring(file:read('*all'))
		if ok then
   			self.mDB = self:InitBase(data)
   			self.mCache = {}
   			self.mCacheIcons = {}
   			print(aPath .. ' loaded;')
   		else
   			printError(aPath .. ' loading failed: ' .. data)
   		end
    	file:close()
    else
    	printError(aPath .. ' not exists;')
    end
end

function DBEditor:InitBase(aDB)
	local db = aDB or {}

	db.info  = db.info  or {}
	db.types = db.types or {}
	db.macro = db.macro or {}
	db.sheme = db.sheme or {}
	db.data  = db.data  or {}

	if not db.info.tables then
		db.info.tables = {}
	end

	for _,name in pairs(db.info.tables) do
		db.macro[name] = db.macro[name] or ''
		db.sheme[name] = db.sheme[name] or {}
		db.data[name]  = db.data[name]  or {}

		for _,rec in pairs(db.data[name]) do
			for _,col in pairs(db.sheme[name]) do
				if rec[col.name] == nil then
					rec[col.name] = col.default
				end
			end
		end
	end

	local exclude_from_filter = {
		effect = true;
		table  = true;
		map    = true;
	}

	if true then
		self.mTablesData = {}

		for _,name in pairs(db.info.tables) do
			local td = {}

			td.scroll = UnityEngine.Vector2(0,0)
			td.foldouts = {}

			td.foldout_macro = false

			td.fields = {}
			td.fields_map = {}
			for i,col in pairs(db.sheme[name]) do
				table.insert(td.fields, col.name)
				td.fields_map[col.name] = col
			end

			td.filter_map = {}
			td.filter_fields = {}
			for _,col in pairs(db.sheme[name]) do
				if not exclude_from_filter[col.type] then
					table.insert(td.filter_fields, col.name)
					td.filter_map[col.name] = #td.filter_fields - 1
				end
			end
			td.filter_none = #td.filter_fields

			td.filter_array = GUIHelper.PrepareArrayPopup(td.filter_fields)
			td.filter_field = "none"
			td.filter_value = {}
			td.filter_passed = {}
			td.filter_passed_count = 0

			td.highlited_names = {}

			td.current_page = 1

			self.mTablesData[name] = td
		end

		if not db.data[self.mCurrentTable] then
			self.mCurrentTable = db.info.tables[1] or false
		end
	end

	return db
end

function DBEditor:Init()
	self.mPath = 'Assets/LuaComponent/Resources/database.lua.txt'

	self.mCurrentTable = false
	self.mTablesData = {}

	self.mScope = {}
	self.mScopeStr = ''

	self.mDB = self:InitBase()
	self.mCache = {}

	self.mRecordsPerPage = 50
end

function DBEditor:InitGUI()
	if self.mInitGUI then
		return
	end

	local button_skin = UnityEngine.GUI.skin.button
	-- local popup_skin = UnityEditor.EditorStyles.popup

	self.mRecTitleStyle = UnityEngine.GUIStyle(button_skin)
	self.mRecTitleStyle.richText = true

	self.mInitGUI = true
end

function DBEditor:OnGUI()
	self:InitGUI()

	GUILayout.BeginVertical({})
		GUILayout.BeginHorizontal({})
			local curr = GUIHelper.StringPopup('Table:', self.mCurrentTable or 'none', self.mDB.info.tables or {})
			if self.mDB.data[curr] then
				self.mCurrentTable = curr
			end

			if GUILayout.Button("Save", {}) then
				self:Save(self.mPath)
			end
			if GUILayout.Button("Load", {}) then
				self:Load(self.mPath)
			end
		GUILayout.EndHorizontal()

		if self.mCurrentTable then
			local td = self.mTablesData[self.mCurrentTable]
			local ct = self.mDB.data[self.mCurrentTable]
			local sh = self.mDB.sheme[self.mCurrentTable]

			td.foldout_macro = EditorGUILayout.Foldout(td.foldout_macro, 'Macro')
			if td.foldout_macro then
				self.mDB.macro[self.mCurrentTable] = EditorGUILayout.TextArea(self.mDB.macro[self.mCurrentTable], {})
				if GUILayout.Button("Run", {}) then
					self:Query(self.mDB.macro[self.mCurrentTable])
				end
			end

			self:DrawTable(self.mCurrentTable)

			GUILayout.BeginHorizontal({})
				self:PageInput()
				self:FilterInput()

				td.current_page = math.min(math.max(td.current_page, 1), self:GetPageCount())

				GUILayout.FlexibleSpace();

				local str = GUILayout.TextField(self.mScopeStr, { GUILayout.Width(100) })
				if self.mScopeStr ~= str then
					self.mScope = aux.getScopeTable(str)
					self.mScopeStr = str
				end

				if GUILayout.Button("ADD", { GUILayout.Width(60) }) then
					self:AddRecord(ct, sh)
					self.mScope = {}
					self.mScopeStr = ''
				end
				aux.setBackColor('red')
				aux.setContColor('white')
				if GUILayout.Button("DEL", { GUILayout.Width(40) }) then
					self:DelRecord(ct)
					self.mScope = {}
					self.mScopeStr = ''
				end
				aux.resetBackColor()
				aux.resetContColor()
			GUILayout.EndHorizontal()
		end
	GUILayout.EndVertical()
end


function DBEditor:DrawTable(aTableName)
	local td = self.mTablesData[aTableName]
	local ct = self.mDB.data[aTableName]
	local sh = self.mDB.sheme[self.mCurrentTable]

	local record_i = 1
	local from = (td.current_page - 1) * self.mRecordsPerPage
	local to   = td.current_page * self.mRecordsPerPage

	td.scroll = GUILayout.BeginScrollView(td.scroll, {})
		for i,rec in pairs(ct) do
			if td.filter_passed[i] or td.filter_field == "none" then
				if record_i > from and record_i <= to then
					GUILayout.BeginHorizontal({})
						if self.mScope[i] then aux.setColor('green') end
						GUILayout.Label(tostring(i), { GUILayout.Width(25) })
						if self.mScope[i] then aux.resetColor() end

						local content = UnityEngine.GUIContent(td.highlited_names[rec.name] or rec.name)
						content.tooltip = rec.tooltip or ""
						local height = { GUILayout.Height(20) }
						local size = 50
						if rec.icon and rec.icon ~= "none" and rec.icon ~= "" then
							height = { GUILayout.Height(size) }
							if not td.foldouts[i] then
								size = 50
							else
								size = 100
							end
						end
						if GUILayout.Button(content, self.mRecTitleStyle, height) then
							td.foldouts[i] = not td.foldouts[i]
						end

						local rect = UnityEngine.GUILayoutUtility.GetLastRect()
					GUILayout.EndHorizontal()

					GUILayout.BeginHorizontal({})

					GUILayout.Space(size)

					if td.foldouts[i] then
						rect = UnityEngine.GUILayoutUtility.GetLastRect()
					end
					if rec.icon and rec.icon ~= "none" and rec.icon ~= "" then
						rect.width  = size
						rect.height = size

						self:DrawIcon(rect, rec.icon)
					end

					if td.foldouts[i] == true then
						GUILayout.BeginVertical({})
							self:DrawRecord(rec, sh, aTableName)
						GUILayout.EndVertical()
					end
					GUILayout.EndHorizontal()
				end
				record_i = record_i + 1
			end
		end
	GUILayout.EndScrollView()
end

function DBEditor:DrawRecord(aRec, aSheme, aTableName)
	for _,col in pairs(aSheme) do
		local func = typesGUI[col.type]
		if func then
			local new = func(col.name, aRec[col.name], col, self)
			if aRec[col.name] ~= new then
				aRec[col.name] = new
				self:UpdPopupCache(aTableName, col.name)

				if aTableName == 'items' and (col.name == 'name' or col.name == 'section') then
					self:UpdPopupTreeCache(self.mDB.data[aTableName], 'name', 'section')
				end
			end
		end
	end
	-- GUILayout.Box('', { GUILayout.ExpandWidth(true), GUILayout.Height(1) })
	GUILayout.Space(5)
end

function DBEditor:DrawIcon(aRect, aIconPath)
	local at = self.mCacheIcons[aIconPath]
	if not at then
		local t = aIconPath:split('/')
		at = GUIHelper.LoadAtlasTexture("ui/" .. t[1], t[2])
		self.mCacheIcons[aIconPath] = at
	end
	GUIHelper.DrawAtlasTexture(aRect, at)
end


function DBEditor:CreateRecord(aSheme)
	local rec = {}
	for _,col in pairs(aSheme) do
		rec[col.name] = col.default
	end
	return rec
end

function DBEditor:AddRecord(aTable, aSheme)
	local count = 0

	for k in pairs(self.mScope) do
		local rec = self:CreateRecord(aSheme)
		table.insert(aTable, k + count, rec)
		count = count + 1
	end

	if count == 0 then
		local rec = self:CreateRecord(aSheme)
		table.insert(aTable, rec)
	end
end

function DBEditor:DelRecord(aTable)
	local removeLabel = {}

	for k in pairs(self.mScope) do
		aTable[k] = removeLabel
	end

	local i,o,c = 1,1,0

	while aTable[i] ~= nil do
		if aTable[i] == removeLabel then
			c = c + 1
		else
			aTable[o] = aTable[i]
			o = o + 1
		end
		i = i + 1
	end

	for i = #aTable, #aTable-c+1, -1 do
		aTable[i] = nil
	end
end

function DBEditor:Query(aStr)
	local func = loadstring(aStr)
	local env = {}

	env.db = self.mDB

	env.add_record = function(tname, index, vals)
		local ct = self.mDB.data[tname]
		local sh = self.mDB.sheme[tname]

		local rec = self:CreateRecord(sh)
		for k,v in pairs(vals) do
			rec[k] = v
		end

		table.insert(ct, index, rec)
	end

	setmetatable(env, { __index = _G })
	setfenv(func, env)

	if func then
		func()
	end
end

function DBEditor:PageInput()
	local td = self.mTablesData[self.mCurrentTable]

	local total   = self:GetPageCount()
	local current = math.min(td.current_page, total)

	if GUILayout.Button('<', {}) then
		current = math.max(current - 1, 1)
	end

	local text = current .. '/' .. total
	GUILayout.Label(text, {})

	if GUILayout.Button('>', {}) then
		current = math.min(current + 1, total)
	end

	td.current_page = current
end

function DBEditor:GetPageCount()
	local td = self.mTablesData[self.mCurrentTable]
	local ct = self.mDB.data[self.mCurrentTable]

	local records = td.filter_passed_count > 0 and td.filter_passed_count or #ct
	return math.ceil(records / self.mRecordsPerPage)
end

function DBEditor:FilterInput()
	local td = self.mTablesData[self.mCurrentTable]

	local field_num = td.filter_map[td.filter_field] or td.filter_none
	local new = EditorGUILayout.Popup(field_num, td.filter_array, { GUILayout.Width(100) })
	if new ~= field_num then
		td.filter_field = td.filter_array[new]
		local col = td.fields_map[td.filter_field]
		if col then
			td.filter_value = typesGUIInit[col.type]()
			self:Filter()
		end
	end

	if td.filter_field ~= "none" then
		local col = td.fields_map[td.filter_field]
		local new = typesGUI[col.type]('', td.filter_value, col, self)
		if new ~= td.filter_value then
			td.filter_value = new
			self:Filter()
		end
	else
		td.filter_passed_count = 0
		td.highlited_names = {}
	end
end

function DBEditor:Filter()
	local td = self.mTablesData[self.mCurrentTable]
	local ct = self.mDB.data[self.mCurrentTable]

	td.filter_passed = {}
	td.highlited_names = {}
	td.filter_passed_count = 0

	local hightlight = function(s)
		return '<color=cyan>' .. s .. '</color>'
	end

	local col = td.fields_map[td.filter_field]
	if col.type == 'string' then
		if #td.filter_value > 0 then
			local set = {}
			for i = 1, #ct do
				set[i] = ct[i][td.filter_field]
			end

			local matches = fuzzy_match(set, td.filter_value, hightlight)
			for _,match in pairs(matches) do
				local rec = ct[match.index]
				td.filter_passed[match.index] = true
				td.filter_passed_count = td.filter_passed_count + 1
				td.highlited_names[rec.name] = match.highlighted
			end
		else
			table.fill(td.filter_passed, 1, #ct, true)
			td.filter_passed_count = #ct
		end
	else
		if td.filter_value == "none" then
			table.fill(td.filter_passed, 1, #ct, true)
			td.filter_passed_count = #ct
		else
			for i = 1, #ct do
				if td.filter_value == ct[i][td.filter_field] then
					td.filter_passed[i] = true
					td.filter_passed_count = td.filter_passed_count + 1
				end
			end
		end
	end
end

function DBEditor:UpdPopupCache(aTable, aField)
	local td = self.mTablesData[aTable]
	td.popup_cache = td.popup_cache or {}

	local cache = {}

	local data = self.mDB.data[aTable]
	cache.map = {}
	local t = {}
	for i = 1, #data do
		local val = data[i][aField]
		if val then
			t[#t + 1] = val
			cache.map[val] = i - 1
		end
	end
	cache.array = GUIHelper.PrepareArrayPopup(t)

	cache.none = #data

	td.popup_cache[aField] = cache
end

function DBEditor:GetPopupCache(aTable, aField)
	local td = self.mTablesData[aTable]
	td.popup_cache = td.popup_cache or {}

	if not td.popup_cache[aField] then
		self:UpdPopupCache(aTable, aField)
	end

	return td.popup_cache[aField]
end

function DBEditor:UpdPopupTreeCache(aTable, aMainField, aSectionField)
	local cache = {}

	local data = self.mDB.data["items"]
	local valueArray = {}
	local viewArray = {}
	local valueMap = {}
	for i = 1, #data do
		valueArray[i] = data[i][aMainField]
		viewArray[i]  = data[i][aSectionField] .."/".. valueArray[i]
		valueMap[valueArray[i]] = i - 1
	end

	cache.value_array = GUIHelper.PrepareArrayPopup(valueArray)
	cache.view_array = GUIHelper.PrepareArrayPopup(viewArray)
	cache.value_map = valueMap
	cache.none = #data

	self.mCache[aTable] = cache
end

function DBEditor:GetPopupTreeCache(aTable, aMainField, aSectionField)
	if not self.mCache[aTable] then
		self:UpdPopupTreeCache(aTable, aMainField, aSectionField)
	end
	return self.mCache[aTable]
end

-----------------------------------------------------------
--	Aux
-----------------------------------------------------------
function aux.setColor(aColor)
	UnityEngine.GUI.color = UnityEngine.Color[aColor]
end

function aux.resetColor()
	UnityEngine.GUI.color = UnityEngine.Color.white
end

function aux.setBackColor(aColor)
	UnityEngine.GUI.backgroundColor = UnityEngine.Color[aColor]
end

function aux.resetBackColor()
	UnityEngine.GUI.backgroundColor = UnityEngine.Color.white
end

function aux.setContColor(aColor)
	UnityEngine.GUI.contentColor = UnityEngine.Color[aColor]
end

function aux.resetContColor()
	UnityEngine.GUI.contentColor = UnityEngine.Color.white
end

function aux.getScopeTable(aStr)
	local scope = {}

	aStr = aStr:gsub('(%d+)%s*%:%s*(%d+)', function(a,b)
		for i = a, b do
			scope[i] = true
		end
		return ''
	end)

	aStr = aStr:gsub('(%d+)%s*%+%s*(%d+)', function(a,b)
		for i = a, a + b do
			scope[i] = true
		end
		return ''
	end)

	aStr = aStr:gsub('(%d+)%s*%-%s*(%d+)', function(a,b)
		for i = a - b, a do
			scope[i] = true
		end
		return ''
	end)

	aStr:gsub('%d+', function(a)
		scope[tonumber(a)] = true
	end)

	return scope
end


-----------------------------------------------------------
--	typesGUI
-----------------------------------------------------------
typesGUIInit = {}

typesGUIInit['bool']      = function() return false end
typesGUIInit['float']     = function() return 0 end
typesGUIInit['int']       = function() return 0 end
typesGUIInit['string']    = function() return '' end
typesGUIInit['any']       = function() return 0 end
typesGUIInit['enum']      = function() return "none" end
typesGUIInit['base_enum'] = function() return "none" end
typesGUIInit['table']     = function() return {} end
typesGUIInit['map']       = function() return {} end
typesGUIInit['item_num']  = function() return {item="none",number=0} end
typesGUIInit['item_sectioned']  = function() return "none" end
typesGUIInit['effect']    = function() return {} end
typesGUIInit['quest_requirement']    = function() return {"flag",0 } end
typesGUIInit['quest_task']    = function() return {"flag", 0 } end
typesGUIInit['quest_rewards']    = function() return {"flag", 0 } end

typesGUI['bool'] = function(key,val)
	return EditorGUILayout.Toggle(key, val, {})
end

typesGUI['float'] = function(key,val)
	return EditorGUILayout.FloatField(key, val, {})
end

typesGUI['int'] = function(key,val)
	return EditorGUILayout.IntField(key, val, {})
end

typesGUI['string'] = function(key,val)
	return EditorGUILayout.TextField(key, val, {})
end

typesGUI['any'] = function(key,val,col)
	local si = luaToStr(val)
	local so = EditorGUILayout.TextField(key, si, {})

	if so ~= si then
		local vo = strToLua(so)
		if vo ~= nil and luaToStr(vo) == so then
			return vo
		end
		if so == 'nil' then
			return nil
		end
	end

	return val
end

typesGUI['enum'] = function(key,val,col,editor)
	local cache = editor:GetPopupCache(col.table_name, col.field_name)
	local curr = cache.map[val] or cache.none

	curr = EditorGUILayout.Popup(key, curr, cache.array, {})

	return cache.array[curr]
end

typesGUI['base_enum'] = function(key,val,col,editor)
	local db = editor.mDB
	local options
	for i = 1,#db.data.enums do
		if db.data.enums[i].name == col.enum_name then
			options = db.data.enums[i].value
			break
		end
	end
	return GUIHelper.StringPopup(key, val, options)
end

typesGUI['table'] = function(key,val,col,editor)
	GUILayout.BeginHorizontal({})
		GUILayout.Label(key, { GUILayout.Width(150) })
		GUILayout.BeginVertical({})
			val = val or {}
			for i = 1, #val do
				GUILayout.BeginHorizontal({})
				val[i] = typesGUI[col.subtype](tostring(i), val[i],col,editor)
				aux.setBackColor('red')
				aux.setContColor('white')
				if GUILayout.Button('Remove', {}) then
					table.remove(val, i)
					aux.resetBackColor()
					aux.resetContColor()
					break
				end
				aux.resetBackColor()
				aux.resetContColor()
				GUILayout.EndHorizontal()
			end
			GUILayout.BeginHorizontal({})
				if GUILayout.Button('Add', {}) then
					val[#val + 1] = typesGUIInit[col.subtype]()
				end
				GUILayout.EndHorizontal()
		GUILayout.EndVertical()
	GUILayout.EndHorizontal()
	return val
end

typesGUI['map'] = function(key,val,col,editor)
	val = val or {}

	local move = {}
	for k,v in pairs(val) do
		local newk = EditorGUILayout.TextField( k, {})
		if newk ~= k then
			move[k] = newk
		end
		val[k] = typesGUI[col.subtype](k,v,col,editor)
	end

	for old,new in pairs(move) do
		val[new] = val[old]
		val[old] = nil
	end

	if GUILayout.Button('add', {}) then
		val['__new'] = typesGUIInit[col.subtype]()
	end
	return val
end

typesGUI['item_num'] = function(key,val,col,editor)
	val = val or {}
	EditorGUILayout.BeginHorizontal({})
	val.item = typesGUI.item_sectioned(key,val.item,col,editor)
	GUILayout.Label("value", {})
	val.number = EditorGUILayout.FloatField(val.number, {GUILayout.MaxWidth(80)})
	EditorGUILayout.EndHorizontal()
	return val
end

typesGUI['item_sectioned'] = function(key,val,col,editor)
	local db = editor.mDB
	local data = db.data["items"]

	local cache = editor:GetPopupTreeCache(data, 'name', 'section')
	local curr = cache.value_map[val] or cache.none

	curr = EditorGUILayout.Popup(key, curr, cache.view_array, {})

	return cache.value_array[curr]
end

typesGUI['effect'] = function(key,val,col,editor)
	val = val or {}
	GUILayout.BeginVertical({})
	val.type       = typesGUI.base_enum('type',val.type,{enum_name='EffectTypes'}, editor)

	if val.type == "GiveItemsNum" or val.type == "GiveItemsChance" then

		val.itemsGive = typesGUI.item_sectioned("item",val.item,col,editor)
		val.power      = EditorGUILayout.FloatField("power", val.power or 1, {})
	elseif  val.type == "GiveRandomItemsFromSection" then
		val.section    = typesGUI.base_enum('section',val.section,{enum_name='ItemSections'}, editor)
		val.power      = EditorGUILayout.FloatField("power", val.power or 1, {})
	elseif  val.type == "SetFlags" then
		EditorGUILayout.BeginHorizontal({})
		val.flagsValue = val.flagsValue or {}
		val.flagsValue.flag = EditorGUILayout.TextField("flag", val.flagsValue.flag or "", {})
		val.flagsValue.value = EditorGUILayout.TextField(val.flagsValue.value or "", {})
		EditorGUILayout.EndHorizontal()
	elseif  val.type == "IncreaseCraftSpeed" or val.type == "IncreaseExpGain" or val.type == "IncreaseGoldGain" or val.type == "IncreaseRewardsDropChance" or val.type == "DecreaseEnergyDelay" or val.type == "DecEnergyConsume" then
		val.duration   = EditorGUILayout.FloatField("duration", val.duration or 0, {})
		val.power      = EditorGUILayout.FloatField("power", val.power or 1, {})
	elseif  val.type == "WearExpInc" or val.type == "WearTimeMGHO" or val.type == "WearEnergyDelayDec" or val.type == "WearGoldInc" or val.type == "WearCraftSpeedInc" or val.type == "WearEnergyInc" or val.type == "WearAccessTimeDec"or val.type =="DecEnergyConsume" then

		val.duration   = EditorGUILayout.FloatField("duration", val.duration or 0, {})
		val.power      = EditorGUILayout.FloatField("power", val.power or 1, {})
	elseif  val.type == "Energy" or val.type =="Maxenergy" then
		val.power      = EditorGUILayout.FloatField("power", val.power or 1, {})
	end

	-- val.icon       = "items.item_shop_water_dragon_amulet"

	GUILayout.EndVertical()
	GUILayout.Space(3)
	return val
end

typesGUI['quest_requirement'] = function(key,val,col,db,editor)
	val = val or {}
	EditorGUILayout.BeginHorizontal({})
	-- val[1] = typesGUI.enum('flag',val[1] , {table_name="flags",field_name="name"}, db,editor)
	val[1] = EditorGUILayout.TextField("Flag", val[1] or "" , {})
	val[2] = EditorGUILayout.TextField("Value", val[2] or "" , {})
	-- val[2] = typesGUI.any('Value',val[2] , col, db,editor)
	EditorGUILayout.EndHorizontal()
	GUILayout.Space(4)
	return val
end

typesGUI['quest_task'] = function(key,val,col,db,editor)
	val = val or {}
	GUILayout.BeginVertical({})
	-- val[1]   = typesGUI.enum('Flag', val[1], {table_name="flags",field_name="name"}, db, editor)
	val[1] = EditorGUILayout.TextField("Flag", val[1] or "" , {})
	val.usestartvalue = EditorGUILayout.Toggle("Use start value", val.usestartvalue or false, {})
	val.targetvalue = EditorGUILayout.TextField('Target value',val.targetvalue or "", {})
	local target = ""
	if val.usestartvalue then
		val.operation = GUIHelper.StringPopup("Operation", val.operation, {"+","-","*",":"})
		local op = val.operation
		if op == ":"then
			op = "/"
		end
		target = "start"..op..val.targetvalue
	else
		target = val.targetvalue
	end
	val[2] = target
	val.take = EditorGUILayout.Toggle("Take", val.take or false, {})

	-- val.icon = typesGUI.enum('Icon selector', val.icon, {table_name="icons",field_name="name"}, db, editor)
	val.icon = EditorGUILayout.TextField("Icon", val.icon , {})
	val.location = typesGUI.enum('Move Location',val.location, {table_name="levels",field_name="name"}, db,editor)
	val.targetlocation = typesGUI.enum('Target Location',val.targetlocation, {table_name="levels",field_name="name"}, db,editor)
	val.openWindow =  EditorGUILayout.TextField("OpenWindowID", val.openWindow or "" , {})
	if val.openWindow ~= "" then
		val.openWindowParams = typesGUI['table']("WindowParams",val.openWindowParams, {subtype = "any"} ,editor)
	end
	val.hiden = EditorGUILayout.Toggle("hiden", val.hiden  , {})

	GUILayout.EndVertical()
	GUILayout.Space(4)
	return val
end


typesGUI['quest_rewards'] = function(key,val,col,db,editor)
	val = val or {}
	GUILayout.BeginVertical({})
	-- val[1]   = typesGUI.enum('flag', val[1], {table_name="flags",field_name="name"}, db, editor)
	val[1] = EditorGUILayout.TextField("Flag", val[1] or "" , {})
	val.usestartvalue = EditorGUILayout.Toggle("usestartvalue", val.usestartvalue or false, {})
	val.targetvalue = EditorGUILayout.TextField('targetvalue',val.targetvalue or "", {})
	local target = ""
	if val.usestartvalue then
		val.operation = GUIHelper.StringPopup("operation", val.operation, {"+","-","*",":"})
		local op = val.operation
		if op == ":"then
			op = "/"
		end
		target = "current"..op..val.targetvalue
	else
		target = val.targetvalue
	end
	val[2] = target

	-- val.icon = typesGUI.enum('Icon selector', val.icon, {table_name="icons",field_name="name"}, db, editor)
	val.icon = EditorGUILayout.TextField("Icon", val.icon , {})
	GUILayout.EndVertical()
	GUILayout.Space(4)
	return val
end
